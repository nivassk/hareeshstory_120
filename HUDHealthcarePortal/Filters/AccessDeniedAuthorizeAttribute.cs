﻿
using System;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;
using HUDHealthcarePortal.Core;

namespace HUDHealthcarePortal.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class AccessDeniedAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (filterContext.HttpContext.User != null && (filterContext.HttpContext.User.Identity.IsAuthenticated &&
                                                           filterContext.Result is HttpUnauthorizedResult))
            {
                filterContext.Result =
                    new RedirectToRouteResult(
                        new RouteValueDictionary(new { Controller = "Home", Action = "AccessDenied" }));

                var message = String.Format(CultureInfo.InvariantCulture,
                    "Access denied for UserName: {0}, View: {1}", UserPrincipal.Current.UserName, filterContext.HttpContext.Request.RawUrl);
                UnauthorizedAccessException exception = new UnauthorizedAccessException(message);
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }
    }
}