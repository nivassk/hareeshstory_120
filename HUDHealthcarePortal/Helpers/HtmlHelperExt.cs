﻿using HUDHealthcarePortal.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace HUDHealthCarePortal.Helpers
{
    public static class HtmlHelpers
    {
        public static IHtmlString SimpleLink(this HtmlHelper html, string url, string text)
        {
            return new HtmlString(string.Format(@"<a href='{0}' target='_blank'>{1}</a>", url, text));
        }

        public static string Currency(this HtmlHelper html, decimal data)
        {
            var culture = new CultureInfo("en-US");
            return data.ToString("C0", culture);
        }

        public static IHtmlString PopupLink(this HtmlHelper html, string windowName, string displayName, string paramId)
        {
            var anchor = new TagBuilder("a");
            //anchor.AddCssClass("class");
            anchor.Attributes["src"] = "#";
            anchor.Attributes["onclick"] = string.Format(
                "g_modal_paramId={0}; $('#{1}').dialog('open'); return false;", paramId, windowName);
            anchor.SetInnerText(displayName);
            return new HtmlString(anchor.ToString());
        }
        public static IHtmlString PopupLink2(this HtmlHelper html, string displayName, string paramId)
        {
            var anchor = new TagBuilder("a");
            //anchor.AddCssClass("class");
            anchor.Attributes["src"] = "#";
            anchor.Attributes["onclick"] = string.Format("openPopupLink({1},'{0}'); return false;", paramId, "'@TempData['" + "paramId" + "']'");
                //"debugger;@TempData['paramId']={0}; $('#dialogCommon').dialog('open'); return false;", paramId);
            anchor.SetInnerText(displayName);
            return new HtmlString(anchor.ToString());
        }
        public static IHtmlString PopupLink3(this HtmlHelper html, string displayName, string paramId)
        {
            var anchor = new TagBuilder("a");
            //anchor.AddCssClass("class");
            anchor.Attributes["src"] = "#";
            anchor.Attributes["onclick"] = string.Format(
                "debugger;$('#dialogCommon').dialog('open'); return false;", paramId);
            anchor.SetInnerText(displayName);
            return new HtmlString(anchor.ToString());
        }

        public static string AbsoluteAction(this UrlHelper url, string actionName, string controllerName, object routeValues = null)
        {
            string scheme = url.RequestContext.HttpContext.Request.Url.Scheme;
            return url.Action(actionName, controllerName, routeValues, scheme);
        }

        public static string AbsoluteAction(ControllerContext context, string actionName, string controllerName, object routeValues = null)
        {
            string scheme = context.RequestContext.HttpContext.Request.Url.Scheme;
            var urlHelper = new UrlHelper(context.RequestContext);
            return urlHelper.Action(actionName, controllerName, routeValues, scheme);
        }

        public static IHtmlString ButtonAction(ControllerContext context, string strController, string strAction, string strText)
        {
            string link = AbsoluteAction(context, strAction, strController);
            return new HtmlString(string.Format("<input type='button' value='{0}' onclick='location.href=\"{1}\"' />", strText, link));
        }

        public static MvcHtmlString ImageActionLink(
           this HtmlHelper helper,
           string imageUrl,
           string altText,
           string actionName,
           string controllerName,
           object routeValues,
           object linkHtmlAttributes,
           object imgHtmlAttributes)
        {
            var linkAttributes = AnonymousObjectToKeyValue(linkHtmlAttributes);
            var imgAttributes = AnonymousObjectToKeyValue(imgHtmlAttributes);
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", altText);
            imgBuilder.MergeAttributes(imgAttributes, true);
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var linkBuilder = new TagBuilder("a");
            //tack on a GUID to prevent AJAX caching
            var routeDictionary = new RouteValueDictionary(routeValues);
            routeDictionary.Add("guid", Guid.NewGuid());
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, controllerName, routeDictionary));
            linkBuilder.MergeAttributes(linkAttributes, true);
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        public static MvcHtmlString ImageActionLink(
           this HtmlHelper helper,
           string imageUrl,
           string altText,
           string actionName,
           string controllerName,
           object routeValues,
           object linkHtmlAttributes,
           object imgHtmlAttributes,
           string fragment)
        {
            var linkAttributes = AnonymousObjectToKeyValue(linkHtmlAttributes);
            var imgAttributes = AnonymousObjectToKeyValue(imgHtmlAttributes);
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", altText);
            imgBuilder.MergeAttributes(imgAttributes, true);
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var linkBuilder = new TagBuilder("a");
            //tack on a GUID to prevent AJAX caching
            var routeDictionary = new RouteValueDictionary(routeValues);
            routeDictionary.Add("guid", Guid.NewGuid());
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, controllerName, routeDictionary) + "#" + fragment);
            linkBuilder.MergeAttributes(linkAttributes, true);
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        public static MvcHtmlString ImageActionLink(
            this HtmlHelper helper,
            string imageUrl,
            string altText,
            string actionName,
            object routeValues,
            object imgHtmlAttributes)
        {
            var imgAttributes = AnonymousObjectToKeyValue(imgHtmlAttributes);
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", altText);
            imgBuilder.MergeAttributes(imgAttributes, true);
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, routeValues));
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        public static MvcHtmlString ImageActionLink(
            this HtmlHelper helper,
            string imageUrl,
            string altText,
            string actionName,
            object routeValues)
        {
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", altText);
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName, routeValues));
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        public static MvcHtmlString ImageActionLink(
            this HtmlHelper helper,
            string imageUrl,
            string altText,
            string actionName)
        {
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", imageUrl);
            imgBuilder.MergeAttribute("alt", altText);
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var linkBuilder = new TagBuilder("a");
            linkBuilder.MergeAttribute("href", urlHelper.Action(actionName));
            var text = linkBuilder.ToString(TagRenderMode.StartTag);
            text += imgBuilder.ToString(TagRenderMode.SelfClosing);
            text += linkBuilder.ToString(TagRenderMode.EndTag);
            return MvcHtmlString.Create(text);
        }

        private static Dictionary<string, object> AnonymousObjectToKeyValue(object anonymousObject)
        {
            var dictionary = new Dictionary<string, object>();
            if (anonymousObject != null)
            {
                foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(anonymousObject))
                {
                    dictionary.Add(propertyDescriptor.Name, propertyDescriptor.GetValue(anonymousObject));
                }
            }
            return dictionary;
        }
    }

    public static class HtmlHelperExt
    {
        public static IHtmlString ScoreColorFormatter(decimal? scoreValue)
        {
            string sColor = scoreValue >= 20 ? "FF7C80" : scoreValue >= 10 ? "FFFF00" : scoreValue >= 0 ? "92D050" : "FFFFFF";
            return new HtmlString(string.Format("<div class='color' style='display: none;'>#" + sColor + "</div><text>" + scoreValue + "</text>"));
        }

        public static IHtmlString ScoreColorFormatter(decimal? scoreValue, string sFormatter)
        {
            if (!scoreValue.HasValue)
                return null;
            string sColor = scoreValue >= 20 ? "FF7C80" : scoreValue >= 10 ? "FFFF00" : scoreValue >= 0 ? "92D050" : "FFFFFF";
            return new HtmlString(string.Format("<div class='color' style='display: none;'>#" + sColor + "</div><text>" + sFormatter + "</text>", scoreValue));
        }

        /// <summary>
        /// negative number displays with red within brackets
        /// </summary>
        /// <param name="value"></param>
        /// <param name="sFormatter"></param>
        /// <returns></returns>
        public static IHtmlString NumberFormatter(decimal? value, string sFormatter)
        {
            if (!value.HasValue)
                return new HtmlString(string.Empty);
            string numberFormat = string.Format("#,##{0}; (#,##{0})", sFormatter);
            string sColor = value < 0 ? @"neg-number" : string.Empty;
            return new HtmlString(string.Format("<span class='{0}'>{1}</span>", sColor, value.Value.ToString(numberFormat)));
        }

        public static IHtmlString CurrencyFormatter(decimal? value, string sFormatter)
        {
            if (!value.HasValue)
                return new HtmlString(string.Empty);
            string numberFormat = string.Format("$#,##{0}; ($#,##{0})", sFormatter);
            string sColor = value < 0 ? @"neg-number" : string.Empty;
            return new HtmlString(string.Format("<span class='{0}'>{1}</span>", sColor, value.Value.ToString(numberFormat)));
        }

        public static IHtmlString NumberFormatterStr(string sValue, string sFormatter)
        {
            decimal dValue;
            if (decimal.TryParse(sValue, out dValue))
                return NumberFormatter(dValue, sFormatter);
            return new HtmlString(string.Empty);
        }

        public static IHtmlString CurrencyFormatterV2(dynamic value, string sFormatter)
        {
            double OutVal=0.00;
            var temp=string.Empty;
            if (value == null)
                return new HtmlString(string.Empty);

            if (value is string && value.ToString().Contains("<font") && !value.ToString().Contains("%"))
             {
                 temp = ExcelAndPrintHelper.StripHTML(value.ToString());
                 double.TryParse(temp.ToString(), out OutVal);
                 var formated = String.Format("{0:" + "#,##0.00}", OutVal);

                 return new HtmlString(string.Format("<span class='{0}'>{1}</span>", "", "<font color='red'>" + formated + "</font>"));

             }
           // value = ExcelAndPrintHelper.StripHTML(value.ToString());
            
            
          

            if (value is string && value.ToString().Contains("-") || value.ToString().Contains("%"))
                return new HtmlString(value);
            if (value is string && !value.ToString().Contains("-") || !value.ToString().Contains("%"))
            {

                double.TryParse(value.ToString(), out OutVal);
            }

           // string numberFormat = string.Format("#,##{0}; (#,##{0})", sFormatter);
            //string numberFormat = "{0:#,##0.00}";
            //string sColor = value < 0 ? @"neg-number" : string.Empty;
            var formatresult = String.Format("{0:" + "#,##0.00}", OutVal);
            return new HtmlString(string.Format("<span class='{0}'>{1}</span>", "", formatresult));
           
        }

        public static IHtmlString CurrencyFormatterStr(string sValue, string sFormatter)
        {
            decimal dValue;
            if (decimal.TryParse(sValue, out dValue))
                return CurrencyFormatter(dValue, sFormatter);
            return new HtmlString(string.Empty);
        }

        public static IHtmlString ShowZeroAsEmpty(string sValue)
        {
            if(sValue == "0")
                return new HtmlString(string.Empty);
            else
                return new HtmlString(sValue);
        }

        public static IHtmlString TruncateToHtmlString(string input, int length)
        {
            if (string.IsNullOrEmpty(input))
            {
                return  new HtmlString(string.Empty);
            }
            else if (input.Length <= length)
            {
                return new HtmlString(input);
            }
            else
            {
                return new HtmlString(input.Substring(0, length) + "<text>...</text>");
            }
        }

        public static string TruncateToString(string input, int length)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            else if (input.Length <= length)
            {
                return input;
            }
            else
            {
                return input.Substring(0, length) + "...";
            }
        }

        public static IHtmlString PopupLink(string windowName, string displayName, string paramId, string toolTip)
        {
            var anchor = new TagBuilder("a");
            //anchor.AddCssClass("class");
            anchor.Attributes["src"] = "#";
            anchor.Attributes["title"] = toolTip;
            anchor.Attributes["onclick"] = string.Format(
                "g_modal_paramId='{0}'; $('#{1}').dialog('open'); return false;", paramId, windowName);
            anchor.SetInnerText(displayName);
            return new HtmlString(anchor.ToString());
        }

        public static IHtmlString PopupImageLink(string windowName, string altDisplayName, string paramId, string imageUrl, string toolTip)
        {
            var anchor = new TagBuilder("a");
            //anchor.AddCssClass("class");
            anchor.Attributes["src"] = "#";
            anchor.Attributes["title"] = toolTip;
            anchor.Attributes["onclick"] = string.Format(
                "g_modal_paramId='{0}'; $('#{1}').dialog('open'); return false;", paramId, windowName);

            //string imgUrl = urlHelper.Content(imgSrc);
            TagBuilder imgTagBuilder = new TagBuilder("img");
            imgTagBuilder.Attributes["src"] = imageUrl;
            imgTagBuilder.Attributes["alt"] = altDisplayName;
            string img = imgTagBuilder.ToString(TagRenderMode.SelfClosing);
            //anchor.SetInnerText(img);
            anchor.InnerHtml = img;
            return new HtmlString(anchor.ToString());
        }

        public static IHtmlString DateFormatter(DateTime value, string sFormatter)
        {
            return new HtmlString(value.ToString(sFormatter));
        }

        public static IHtmlString DateFormatterStr(string sDateTime, string sFormatter)
        {
            DateTime dt;
            if (DateTime.TryParse(sDateTime, out dt))
                return DateFormatter(dt, sFormatter);
            return new HtmlString(string.Empty);
        }

        public static IHtmlString DateFormatterStr(DateTime sDateTime, string sFormatter)
        {
        
            return DateFormatter(sDateTime, sFormatter);
            
        }

        public static IHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> html, Expression<Func<TModel, TEnum>> expression, object htmlAttributes)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var enumType = Nullable.GetUnderlyingType(metadata.ModelType) ?? metadata.ModelType;
            var enumValues = Enum.GetValues(enumType).Cast<object>();
            var items = enumValues.Select(item =>
            {
                var type = item.GetType();
                var member = type.GetMember(item.ToString());
                var attribute = member[0].GetCustomAttribute<DescriptionAttribute>();
                string text = attribute != null ? attribute.Description : item.ToString();
                string value = ((int)item).ToString(CultureInfo.InvariantCulture);
                bool selected = item.Equals(metadata.Model);
                return new SelectListItem
                {
                    Text = text,
                    Value = value,
                    Selected = selected
                };
            });
            return html.DropDownList(expression.ToString(), items, htmlAttributes);
        }

        public static MvcHtmlString  CustomValidationSummary(this HtmlHelper htmlHelper)
        {
            var original = htmlHelper.ValidationSummary();
            return System.Web.Mvc.MvcHtmlString.Create(HttpUtility.HtmlDecode(original.ToString())); 
        }
    }
}