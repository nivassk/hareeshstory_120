﻿using Core;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Helpers
{
    public static class SelectionItemHelper
    {
        public static IEnumerable<SelectListItem> GetSelectItemsFrom(IList<EnumType> enumTypes)
        {
            foreach (var item in enumTypes.ToList())
                yield return new SelectListItem()
                {
                    Value = item.Key,
                    Text = item.Value
                };
        }

        public static IEnumerable<SelectListItem> GetSelectItemsFrom(IEnumerable<KeyValuePair<int, string>> keyValuePairs)
        {
            foreach (var item in keyValuePairs.ToList())
                yield return new SelectListItem()
                {
                    Value = item.Key.ToString(),
                    Text = item.Value
                };
        }
    }
}