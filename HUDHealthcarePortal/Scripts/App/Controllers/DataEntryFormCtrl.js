﻿var app = angular.module('ngdataEntryForm', ['ngTouch', 'ui.grid', 'ngMessages']);

app.controller('DataEntryController', ['$scope', '$http',
    function ($scope, $http) {

        $scope.availableProjectActions = null;
        $scope.availableFormTypes = null;
    /*var getData = dataEntryFormService.GetAllProjectActions();
    getData.then(function(result) {
        $scope.availableProjectActions = result;
    });*/
    
    $scope.gridOptions = {
        rowHeight: 45
    };


    
    $scope.gridOptions.columnDefs =   
         [
            { name: 'Check List Id', field: 'CheckListId' },
            { name: 'Section Id', field: 'SectionId'},
            { name: 'Section Name', field: 'SectionName'},
            { name: 'Question Id', field:'QuestionId' },
            { name: 'Question', field:'Question', width:300 },
            { name: 'Short Names', field: 'Shortnames'},
            { name: 'Level' },
            { field: 'StartDateString', displayName: 'Start Date', type: 'text', enableCellEdit: true, cellFilter: 'date:"dd-MMM-yyyy"' },
               { field: 'DisplayOrderId', name: 'Display OrderId' },
            { name: 'Is Attachment Required', field: 'IsAttachmentRequired' },
            {
                name: 'Edit Question',
                cellTemplate: '<div class="ui-grid-cell-contents"><button ng-click="grid.appScope.editQuestion(row.entity)" aria-label="Left Align">Edit</button></div>'
            }
        ];
    
        $scope.fillFormTypes = function() {
            $http({
                method: 'GET',
                url: '/Account/GetAllFormTypes'
            }).success(function (result) {
                $scope.availableFormTypes = result;
            });
        }
        $scope.fillProjectActions = function (idvalue) {
         $http({
             method: 'POST',
             url: '/Account/GetAllProjectActions',
             data: JSON.stringify({ pageTypeId: idvalue })
         }).success(function (result) {
             $scope.availableProjectActions = result;
         });
     };
     $scope.fillFormTypes();
     //$scope.fillProjectActions();


     $scope.fillQuestionsList = function (idvalue) {
         
             $http({
                 method: 'POST',
                 url: '/Account/GetAllQuestions',
                 data: JSON.stringify({ projectActionTypeId: idvalue })
             }).success(function (result) {
                 $scope.gridOptions.data = result;

                 $scope.divQuestionGrid = true;
             });
         
     };
     

     $scope.AddQuestionForm = function () {
         $scope.Action = "Add";
         $scope.divQuestionForm = true;
         $scope.CheckListId = generateUUID();
         $scope.ProjectActionId = $scope.paSelect;
         $scope.PageTypeId = $scope.ftSelect;
         $scope.QuestionId = "";
         $scope.Question = "";
         $scope.Level = "";
         $scope.DisplayOrderId = $scope.DisplayOrderId + 1;
         $scope.IsAttachmentRequired = "";
         $scope.StartDateString = "";
         $scope.SectionId = "";
         $scope.SectionName = "";
         $scope.Shortnames = "";
         $scope.IsNAForSection = "";
         $scope.IsNARequired = "";
     };

    //function to generate GUID/UUID
     function generateUUID() {
         var d = new Date().getTime();
         if (window.performance && typeof window.performance.now === "function") {
             d += performance.now();; //use high-precision timer if available
         }
         var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
             var r = (d + Math.random() * 16) % 16 | 0;
             d = Math.floor(d / 16);
             return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
         });
         return uuid;
     }

     $scope.AddUpdateEmployee = function() {
            var questionViewModel = {
                CheckListId: $scope.CheckListId,
                SectionId: $scope.SectionId,
                SectionName: $scope.SectionName,
                QuestionId: $scope.QuestionId,
                Question: $scope.Question,
                Shortnames: $scope.Shortnames,
                Level: $scope.Level,
                ProjectActionId: $scope.ProjectActionId,
                DisplayOrderId: $scope.DisplayOrderId,
                ParentQuestionId: $scope.ParentQuestionId,
                IsAttachmentRequired: $scope.IsAttachmentRequired,
                IsNARequired: $scope.IsNARequired,
                IsNAForSection: $scope.IsNAForSection,
                StartDate: $scope.StartDateString,
                StartDateString :  $scope.StartDateString
            };
            var getAction = $scope.Action;

         if (getAction == "Update") {
             questionViewModel.Id = $scope.QuestionId;
             // getData = questionService.updateQuestion(questionViewModel);
             var getData = $http({
                 method: "post",
                 url: "/Account/UpdateQuestion",
                 data: JSON.stringify(questionViewModel),
                 dataType: "json"
             });
             getData.then(function(msg) {
                 $scope.fillQuestionsList($scope.paSelect);
                 alert("Question updated successfully");
                 $scope.divQuestionForm = false;
             }, function() {
                 alert('Error in updating record');
             });
         } else {
             $http({
                 method: "post",
                 url: "/Account/AddQuestion",
                 data: JSON.stringify(questionViewModel),
                 dataType: "json"
             }).success(function(msg) {
                 $scope.fillQuestionsList($scope.paSelect);
                 alert("Question added successfully");
                 $scope.divQuestionForm = false;
             });
         }
     }
      
     $scope.editQuestion = function (row) {
        
         var questionViewModel = row;
         var getData = $http({
             method: "post",
             url: "/Account/GetQuestionById",
             data: JSON.stringify(questionViewModel),
             dataType: "json"
         });

         getData.then(function (question) {
             $scope.question = question.data;
             $scope.CheckListId = question.data.CheckListId;
             $scope.SectionId= question.data.SectionId,
             $scope.SectionName = question.data.SectionName,
             $scope.QuestionId = question.data.QuestionId;
             $scope.Question = question.data.Question;
             $scope.Level = question.data.Level;
             $scope.ProjectActionId = question.data.ProjectActionId;
             $scope.DisplayOrderId = question.data.DisplayOrderId;
             $scope.ParentQuestionId = question.data.ParentQuestionId;
             $scope.IsAttachmentRequired = question.data.IsAttachmentRequired;
             $scope.IsNARequired = question.data.IsNARequired;
             $scope.IsNAForSection = question.data.IsNAForSection;
             $scope.DisplayOrderId = question.data.DisplayOrderId;
             $scope.StartDate = question.data.StartDateString, // question.data.StartDate;
             $scope.StartDateString = question.data.StartDateString;
             $scope.EndDate = question.data.EndDate;
             $scope.Shortnames = question.data.Shortnames;
             $scope.Action = "Update";
             $scope.divQuestionForm = true;
         },
             function () {
                 alert('Error in getting records');
             });
     };
     
    }
]);


