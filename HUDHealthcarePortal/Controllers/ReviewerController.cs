﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BusinessService.Interfaces;
using BusinessService.ProjectAction;
using Core.Utilities;
using Elmah;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthCarePortal.Filters;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Model.ProjectAction;
using Model;
using WebGrease.Css.Extensions;
using RootModel = HUDHealthcarePortal.Model;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Helpers;
using Core;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthCarePortal.Helpers;
using HUDHealthcarePortal.Core.ExceptionHandling;
using System.Configuration;
using System.Web.SessionState;
using BusinessService.Interfaces.UserLogin;
using BusinessService.UserLogin;
using BusinessService;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Web.Mvc.Filters;
using System.Text;




namespace HUDHealthcarePortal.Controllers
{
    public class ReviewerController : Controller
    {
        IAccountManager accountMgr;
        ILookupManager lookupMgr;
        IWebSecurityWrapper webSecurity;
        IReviewerTitlesManager reviewerTitl;
        private ITaskManager taskManager;
        private IQuestionManager questionMgr;
        private IProjectActionManager projectActionMgr;
        private IUserLoginManager ulMgr;
        IReviewerTaskAssignmentManager reviewerTaskAssigmentManager;
        ITaskManager taskMgr;

        public ActionResult Index()
        {
            return View();
        }
        public ReviewerController()
            : this(new AccountManager(), new LookupManager(), new WebSecurityWrapper(), new QuestionManager(), new ProjectActionManager(), new UserLoginManager(), new ReviewerTitlesManager(), new ReviewerTaskAssigmentManager(), new TaskManager())
        {
        }

        /// <param name="webSec">The web sec.</param>
        public ReviewerController(IAccountManager accountManager, ILookupManager lookupManager, IWebSecurityWrapper webSec, IQuestionManager QuestMgr, IProjectActionManager projectActionManager, IUserLoginManager ulManager, IReviewerTitlesManager ReviewerTitlesManager, IReviewerTaskAssignmentManager reviewTaskAssignmentManager, ITaskManager taskManager)
        {
            accountMgr = accountManager;
            lookupMgr = lookupManager;
            webSecurity = webSec;
            questionMgr = QuestMgr;
            projectActionMgr = projectActionManager;
            ulMgr = ulManager;
            reviewerTitl = ReviewerTitlesManager;
            reviewerTaskAssigmentManager = reviewTaskAssignmentManager;
            taskMgr = taskManager;
        }

        [HttpGet]
        [AllowAnonymous]
        //Commented out for Issue PT-206
        //[MvcSiteMapNode(Title = "Task Detail", ParentKey = "TaskId")]
        //[SiteMapTitle("Registration", Target = AttributeTarget.CurrentNode)]
        public ActionResult RegisterReviewer(string taskInstanceId)
         {
             string Role = "Reviewer";
             var model = new ReviewerTaskAssigmentViewModel();
             model.FHANumber = "";
             model.RoleName = Role;
             model.TaskInstanceId = new Guid(taskInstanceId);
             model.ModifiedBy = 0;
             model.PropertyName = "";
             model.fromTaskReAssignment = true;
             model.TaskName = "";
             model.id = 0;
             PopulateTimeZoneList(model);
             List<ReviewerTitlesModel> titles = new List<ReviewerTitlesModel>();
             if (Role == HUDRole.Reviewer.ToString("g"))
             {
                 titles = reviewerTitl.GetReviewerTitles(2);
             }
             else
             {
                 titles = reviewerTitl.GetReviewerTitles(1);
             }
             titles.ToList().ForEach(p => model.AllTitles.Add(new SelectListItem() { Text = p.Reviewer_Name, Value = p.Reviewer_Id.ToString() }));
             PopulateAllStateList(model);

             return View("~/Views/Reviewer/RegisterReviewer.cshtml", model);
        }

        [HttpGet]
        [AllowAnonymous]
        [SiteMapTitle("Registration", Target = AttributeTarget.CurrentNode)]
        public ActionResult RegisterUserWithTitle(string Role)
        {

            var model = new ReviewerTaskAssigmentViewModel();
            model.FHANumber = "";
            model.RoleName = Role;
            model.TaskInstanceId = Guid.Empty;
            model.ModifiedBy =0;
            model.PropertyName = "";
            model.TaskName = "";
            model.id = 0;
            PopulateTimeZoneList(model);
            List<ReviewerTitlesModel> titles = new List<ReviewerTitlesModel>();
            if (Role == HUDRole.Reviewer.ToString("g"))
            {
                titles = reviewerTitl.GetReviewerTitles(2);
            }
            else
            {
                titles = reviewerTitl.GetReviewerTitles(1);
            }
            titles.ToList().ForEach(p => model.AllTitles.Add(new SelectListItem() { Text = p.Reviewer_Name, Value = p.Reviewer_Id.ToString() }));
            PopulateAllStateList(model);
            return View("~/Views/Reviewer/RegisterReviewer.cshtml", model);
        }



        private static void PopulateTimeZoneList(ReviewerTaskAssigmentViewModel model)
        {
            var timezones = SelectionItemHelper.GetSelectItemsFrom(EnumType.GetEnumList(typeof(HUDTimeZone)));
            model.AllTimezones = timezones.ToList();
        }

        private void PopulateAllStateList(ReviewerTaskAssigmentViewModel model)
        {
            accountMgr.GetAllStates()
                .ToList()
                .ForEach(p => model.AllStates.Add(new SelectListItem() { Text = p.Value, Value = p.Key }));
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RegisterReviewer(ReviewerTaskAssigmentViewModel model, Guid? taskInstanceId, string fhaNumber, string TaskName, string Role, int ModifiedBy, string PropertyName)
        {
            //umesh
         
            var model2 = new ReviewerTaskAssigmentViewModel();
            string ServicerComments = string.Empty;
            string FHANumber = string.Empty;
            if(Role==HUDRole.Reviewer.ToString("g"))
            {
                 ServicerComments = Request.Form["ServicerComments"];
            }
          
            var SelectedRoles = new List<string>();
            SelectedRoles.Add(model.RoleName);

            if (webSecurity.UserExists(model.UserName))
            {
                ModelState.AddModelError("UserExistsErr", "User with same email address already exists.");
            }

            if (ModelState.IsValid)
            {

                // Attempt to register the user
                try
                {
                    // randomly generate strong password, 9 characters long
                    model.Password = RandomPassword.Generate(9);
                    string userToken = accountMgr.CreateUser(model);

                    int userId = webSecurity.GetUserId(model.UserName);

                    if (Role == HUDRole.Reviewer.ToString("g"))
                    {
                        model.Status = (int)ReviewerStatus.UnderReview;

                    model.ReviwerUserId = userId;
                    //Insert into ReviwerTaskAssignment

                    var formId = reviewerTaskAssigmentManager.AssinTaskToReviewer(model);

                        var taskList = new List<TaskModel>();
                        var task = new TaskModel();
                        //var InstanceId = Guid.NewGuid();
                        var InstanceId = taskInstanceId.Value;
                        var latesttask = reviewerTaskAssigmentManager.GetLatestTaskId(InstanceId);
                        task.TaskInstanceId = InstanceId;
                        task.DataStore1 = latesttask.DataStore1;
                        task.SequenceId = latesttask.SequenceId + 1;
                        task.AssignedBy = UserPrincipal.Current.UserName;
                        task.AssignedTo = model.UserName;
                        task.StartTime = DateTime.UtcNow;
                        task.Notes = ServicerComments ?? string.Empty;
                        task.TaskStepId = (int)TaskStep.UnderReview;
                        taskList.Add(task);
                        var taskFile = new TaskFileModel();
                        Guid taskInstanceID = InstanceId;
                        FHANumber = fhaNumber;
                        int reviewerUserId = userId;
                        int currentUserId = UserPrincipal.Current.UserId;
                        taskMgr.SaveReviewFileStatus(taskInstanceID, reviewerUserId, currentUserId);
                        // save task data
                        taskMgr.SaveTask(taskList, taskFile);
                    }
                   

                    accountMgr.InitForceChangePassword(userId);
                    // save encrypted password history
                    accountMgr.SavePasswordHist(model.UserName, model.Password);

                    //Code to restrict sending Email to the configured Role
                    var Isblock = "FALSE";
                    var BlockRole = string.Empty;
                    string confirmLink = string.Empty;

                    if (ConfigurationManager.AppSettings["BlocUser"] != null)
                    {
                        Isblock = ConfigurationManager.AppSettings["BlocUser"].ToString();
                    }

                    if (Isblock.ToUpper() == "TRUE")
                    {
                        BlockRole = ConfigurationManager.AppSettings["BlockRole"].ToString();

                        if (SelectedRoles.Contains(BlockRole))
                        {
                            accountMgr.ActivateOrInactivateUser(userId, true);
                        }
                        else
                        {
                            var emailMgr = new EmailManager();
                            confirmLink = Url.Action("AgreementConfirm", "Account", new { id = userToken }, Request.Url.Scheme);
                            emailMgr.SendRegisterUserEmail(model, confirmLink);
                        }

                    }
                    else
                    {
                        var emailMgr = new EmailManager();
                        confirmLink = Url.Action("AgreementConfirm", "Account", new { id = userToken }, Request.Url.Scheme);
                        emailMgr.SendRegisterUserEmail(model, confirmLink);
                        if(model.TaskInstanceId!=Guid.Empty)
                        {
                            ViewBag.Message = "Reviewer Saved successfully";
                            return RedirectToAction("RegisterReviewer", "Reviewer", new { taskInstanceId = model.TaskInstanceId });
                        }
                        else
                        {
                            return RedirectToAction("ListUsers", "Account");
                        }
                      
                    }
                    //if (Role == HUDRole.Reviewer.ToString("g"))
                    //{
                    //    FHANumber = model.FHANumber;
                    //    string RoleName = model.RoleName;
                    //    Guid? TaskInstanceId = model.TaskInstanceId;
                    //    int modifiedBy = model.ModifiedBy;
                    //    int id = model.id;
                    //    TempData["message"] = "Registered Successfully! You can register another Reviewer or click on MyTasks.";
                    //    return RedirectToAction("RegisterReviewer", "Reviewer", new { Fhanumber = fhaNumber, Role = RoleName, taskInstanceId = TaskInstanceId, ModifiedBy = modifiedBy, PropertyName = PropertyName, id = id });
                    //}
                    
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        ExceptionManager.WriteToEventLog(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State), "Application", EventLogEntryType.Error);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            ExceptionManager.WriteToEventLog(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage), "Application", EventLogEntryType.Error);
                        }
                    }
                    throw;
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException(string.Format("Error from creating user: {0}", e.InnerException), e.InnerException);
                }

            }
            PopulateAllStateList(model);
              List<ReviewerTitlesModel> titles = new List<ReviewerTitlesModel>();
                    if (Role == HUDRole.Reviewer.ToString("g"))
                    {
                         titles = reviewerTitl.GetReviewerTitles(2);
                    }
                    else
                    {
                         titles = reviewerTitl.GetReviewerTitles(1);
                    }
          
            titles.ToList().ForEach(p => model.AllTitles.Add(new SelectListItem() { Text = p.Reviewer_Name, Value = p.Reviewer_Id.ToString() }));
            PopulateTimeZoneList(model);
            return View(model);
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
    }
}
