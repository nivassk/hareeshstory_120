﻿using System.Collections;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Filters;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace HUDHealthCarePortal.Controllers
{
    public class ChartItem
    {
        public ChartItem(string category, int count, string color, string quarter)
        {
            Category = category;
            Count = count;
            Color = color;
            Quarter = quarter;
        }
        public string Category { get; set; }
        public int Count { get; set; }
        public string Color { get; set; }
        public string Quarter { get; set; }
    }

    [Authorize]
    public class ChartController : Controller
    {
        IUploadDataManager uploadDataMgr;

        public ChartController() : this(new UploadDataManager())
        {

        }

        public ChartController(IUploadDataManager uploadDataManager)
        {
            uploadDataMgr = uploadDataManager;
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser,HUDDirector,Attorney")]
        public ActionResult Index()
        {
            var model = new ChartModelUI();
            Session["SelectedQuarter"] = null;
            Session["LenderId"] = null;
            Session["LenderIdFilter"] = null;
            var currentQtr = uploadDataMgr.GetLatestQuarter("value");//"Q1 2014";
            model.ReportingPeriod = uploadDataMgr.GetLatestQuarter("key");
            model.AllQuarters = new List<SelectListItem>();
            model.AllQuarters.Add(new SelectListItem() { Text = uploadDataMgr.GetLatestQuarter("key"), Value = currentQtr, Selected = true });
            if (TempData["ReportingPeriod"] != null)
            {
                currentQtr = TempData["ReportingPeriod"].ToString();
                model.ReportingPeriod = DateHelper.GetSelectedQuarterText(currentQtr);
            }
           
            if (Session["ReportingPeriod"] != null)
            {
                TempData["ReportingPeriod"] = Session["ReportingPeriod"];
                currentQtr = TempData["ReportingPeriod"].ToString();
                Session["ReportingPeriod"] = null;
                model.ReportingPeriod = DateHelper.GetSelectedQuarterText(currentQtr);
            }

            
            model.SelectedQuarter = currentQtr;
            var quarters = uploadDataMgr.GetQuartersList();
            foreach (var qtr in quarters)
            {
                model.AllQuarters.Add(new SelectListItem() { Text = qtr.Key, Value = qtr.Value });
                
            }
            return View("GoogleChart",model);
        }

        [HttpPost]
        public ActionResult GetGoolgeChart(string strSelected)
        {
            int[] ranges = {10, 20, 15000};
            var results = new List<ChartItem>();
            var result = DateHelper.GetValuesFromSelectedQuarter(strSelected);
            if (result != null)
            {
                IEnumerable<ExcelUploadView_Model> uploadedResults = uploadDataMgr.GetUploadedDataByQuarter(
                    result.Item1, result.Item2);
                var query = from value in uploadedResults
                    .Where(p => p.ScoreTotal != null)
                    .Select(p => p.ScoreTotal)
                    group value by ranges.Where(x => value >= x)
                        .DefaultIfEmpty()
                        .Last()
                    into groups
                    select new {Key = groups.Key, Values = groups};
                foreach (var group in query)
                {
                    if (group.Key == 0)
                        results.Add(new ChartItem("Low Risk", group.Values == null ? 0 : group.Values.Count(), "#92D050",
                            strSelected));
                    else if (group.Key == 10)
                        results.Add(new ChartItem("Medium Risk", group.Values == null ? 0 : group.Values.Count(),
                            "#FFFF00",
                            strSelected));
                    else if (group.Key == 20)
                        results.Add(new ChartItem("High Risk", group.Values == null ? 0 : group.Values.Count(),
                            "#FF7C80",
                            strSelected));
                }
            }
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}

