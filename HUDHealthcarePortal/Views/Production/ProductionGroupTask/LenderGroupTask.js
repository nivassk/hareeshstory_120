﻿$(function () {
   // debugger;
    $("#LenderGroupTaskGrid").jqGrid({

        url: "/TaskReAssignment/Index",
        datatype: 'json',
        mtype: 'GroupTask',

        colNames: ['Id', 'Task Name', 'Status', 'Lender', 'Type', 'Role','In Use By','Date & Time created','Submited Date','Unlock'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: false },
            { Key: false, name: 'TaskName', index: 'TaskName', editable: false },
            { Key: false, name: 'Status', index: 'Status', editable: false },
            { Key: false, name: 'Lender', index: 'Lender', editable: false },
            { Key: false, name: 'Type', index: 'Type', editable: false },
            { Key: false, name: 'Role', index: 'Role', editable: false },
            { Key: false, name: 'In Use By', index: 'InUseBy', editable: false },
            { Key: false, name: 'Date & Time created', index: 'CreatedDate', editable: true, formatter: 'date', formatoptions: { newformat: 'm/d/y' } },
            { Key: false, name: 'Submited Date', index: 'SubmitedDate', editable: true, formatter: 'date', formatoptions: { newformat: 'm/d/y' } },
           
            { Key: false, name: 'UnLock', index: 'UnLock', editable: false }],
        pager: jQuery('#pager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'Group Task',
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false

    }).navGrid('#pager', { edit: false, add: false, del: false, search: true, refresh: true },
     

{
    //Delete options
    zIndex: 100,
    url: '/TodoList/Delete',
    closeOnEscape: true,
    CloseAfterEdit: true,
    recreateForm: true,
    msg: "Are you sure you want to delete this task?",
    afterComplete: function (response) {
        if (response.responseText) {
            alert(response.responseText);
        }

    }
});

});