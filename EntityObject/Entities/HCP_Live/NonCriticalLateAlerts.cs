﻿namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class NonCriticalLateAlerts
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NonCriticalLateAlertId { get; set; }
        public int PropertyID { get; set; }
        public int LenderID { get; set; }
        public string FHANumber { get; set; }
        public int TimeSpanId { get; set; }
        public int HCP_EmailId { get; set; }
        public string RecipientIds { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}