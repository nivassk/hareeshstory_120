namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SecurityQuestion")]
    public partial class SecurityQuestion
    {
        public SecurityQuestion()
        {
            User_SecurityQuestion = new HashSet<User_SecurityQuestion>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SecurityQuestionID { get; set; }

        [Required]
        [StringLength(100)]
        public string SecurityQuestionDescription { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public virtual ICollection<User_SecurityQuestion> User_SecurityQuestion { get; set; }
    }
}
