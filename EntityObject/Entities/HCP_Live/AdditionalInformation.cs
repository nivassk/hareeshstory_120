﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
     [Table("AdditionalInformation")]
    public class AdditionalInformation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid AdditionalInformationId { get; set; }
        [Required]
        public Guid TaskInstanceId { get; set; }
        public string AEComment { get; set; }
        public string LenderComment { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [Required]
        public int CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? OrderNumber { get; set; }
        [Required]
        public Guid ParentFormID { get; set; }
    }
}
