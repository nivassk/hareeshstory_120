namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HUD_WorkLoad_Manager
    {
        public HUD_WorkLoad_Manager()
        {
            ProjectInfoes = new HashSet<ProjectInfo>();
            User_WLM_PM = new HashSet<User_WLM_PM>();
            HUD_Project_Manager = new HashSet<HUD_Project_Manager>();
        }

        [Key]
        public int HUD_WorkLoad_Manager_ID { get; set; }

        [Required]
        [StringLength(100)]
        public string HUD_WorkLoad_Manager_Name { get; set; }

        public int? AddressID { get; set; }

        public DateTime ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public virtual Address Address { get; set; }

        public virtual ICollection<ProjectInfo> ProjectInfoes { get; set; }

        public virtual ICollection<User_WLM_PM> User_WLM_PM { get; set; }

        public virtual ICollection<HUD_Project_Manager> HUD_Project_Manager { get; set; }
    }
}
