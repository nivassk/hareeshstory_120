﻿namespace EntityObject.Entities.HCP_task
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("Prod_FormAmendmentTask")]
	public partial class Prod_FormAmendmentTask
	{
		[Key]
		public int AmendmentTemplateID { get; set; }
		public Guid? TaskInstanceId { get; set; }
		public Guid? TaskXrefid { get; set; }
		public string FHANumber { get; set; }
		public int PageTypeId { get; set; }
		public DateTime? FirmAmendmentIssueDate { get; set; }
		public string LenderAddressee { get; set; }
		public string AddresseeTitle { get; set; }
		public string LenderCompanyName { get; set; }
		public int FirmAmendmentNum { get; set; }
		public string ProjectName { get; set; }
		public string ProjectAddress { get; set; }
		public string Borrower { get; set; }
		public int Saluatation { get; set; }
		public string LastName { get; set; }
		//public string FirstName { get; set; }
		public DateTime? FirmCommitmentSignedDate { get; set; }
		public int Party_Property_Id { get; set; }
		public string Party_Propert_Name { get; set; }
		public int Party_Property_Address_Id { get; set; }
		public string Party_Property_Address_Name { get; set; }
		public decimal LoanAmount { get; set; }
		public decimal PrevLoanAmount { get; set; }
		public decimal InterestRate { get; set; }
		public decimal MonthlyPayment { get; set; }
		public decimal DifferentMonthlyPayment { get; set; }
		public int DueYear { get; set; }
		public int DueMonth { get; set; }
		public DateTime? CommitmentTerminationDate { get; set; }
		public int AdditionalInitialDeposit { get; set; }
		public decimal R4RAmount { get; set; }
		public decimal CriticalRepairCost { get; set; }
		public decimal RemainingRepairCostExihibitC { get; set; }
		public int EscrowEstimate { get; set; }
		public int RepairCostEstimate { get; set; }
		public int SpecialCondition { get; set; }
		public decimal AnnualLeasePayment { get; set; }
		public int ExhibitLetter { get; set; }
		public string Other { get; set; }
		public string CC_Name { get; set; }
		public string CC_ContactDetails { get; set; }
		public int AuthorizedAgentSignatureId { get; set; }
		public int WLMSignatureId { get; set; }
		public bool AttachExhibitA { get; set; }
		public bool AttachExhibitB { get; set; }
		public bool AttachExhibitC { get; set; }
		public bool AttachHUD92264aORCF { get; set; }
		public DateTime? DAPCompletedDate { get; set; }

		public int CreatedBy { get; set; }
		public int ModifiedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }
	}
}

