﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
     [Table("Prod_Note")]
  public partial  class Prod_Note
    { 
      [Key]
      public int NoteId { get; set; } 
      public Guid TaskInstanceId { get; set; }
      public string Note { get; set; }
      public int CreatedBy { get; set; }
      public DateTime DateCreated { get; set; }
    }
}
