﻿namespace EntityObject.Entities.HCP_task
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("EscrowEstimateType")]
	public partial class EscrowEstimateType
	{
		[Key]
		public int EscrowEstimateTypeId { get; set; }
		public string EscrowEstimate { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}



