namespace EntityObject.Entities.HCP_task
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TaskFlow")]
    public partial class TaskFlow
    {
        public int TaskFlowId { get; set; }

        public int TaskStepId { get; set; }

        public int NxtTaskStepId { get; set; }

        [StringLength(128)]
        public string UserRole { get; set; }

        [StringLength(128)]
        public string NxtStepRole { get; set; }

        public virtual TaskStep TaskStep { get; set; }

        public virtual TaskStep TaskStep1 { get; set; }
    }
}
