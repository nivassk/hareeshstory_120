﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject
{
    public class Prod_BorrowerType
    {
        [Key]
        public int BorrowerTypeId { get; set; }
        public string BorrowerTypeName { get; set; }
    }
}
