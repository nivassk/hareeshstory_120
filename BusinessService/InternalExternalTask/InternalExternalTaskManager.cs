﻿using BusinessService.Interfaces.InternalExternalTask;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.InternalExternalTask;
using Repository.Interfaces;
using Repository.Interfaces.InternalExternalTask;
using Repository.ManagementReport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace BusinessService.InternalExternalTask
{
   public class InternalExternalTaskManager : IInternalExternalTaskManager
    {
        private UnitOfWork unitOfWork;
        private IInternalExternalTaskRepository internalExternalTaskRepository;
        private IProjectInfoRepository projectInfoRepository;
        IUserInfoRepository userInfoRepository;

        public InternalExternalTaskManager()
        {
            unitOfWork = new UnitOfWork(DBSource.Live);
            internalExternalTaskRepository = new InternalExternalTaskRepository(unitOfWork);
            projectInfoRepository = new ProjectInfoRepository(unitOfWork);
            userInfoRepository = new UserInfoRepository(unitOfWork);
        }

        public Guid AddInternalExternalTask(InternalExternalTaskModel internalExternalTask)
        {
            internalExternalTask.CreatedDate = DateTime.UtcNow;
            internalExternalTask.CreatedBy = UserPrincipal.Current.UserId;
            internalExternalTask.ModifiedOn = null;
            internalExternalTask.ForwardedDate = null;
            internalExternalTask.ClosingDate = null;

            return internalExternalTaskRepository.AddInternalExternalTask(internalExternalTask);
        }
      
        public InternalExternalTaskModel FindByFHANuber(string fhaNumber)
        {
            return internalExternalTaskRepository.FindByFHANuber(fhaNumber);
        }

        public InternalExternalTaskModel FindByTaskInstanceID(Guid taskInstanceID)
        {
            return internalExternalTaskRepository.FindByTaskInstanceID(taskInstanceID);
        }

        public List<InternalExternalTaskModel> GetAllTasks()
        {
            return internalExternalTaskRepository.GetAllTasks();
        }

        public List<string> GetAvailableFHANumbers()
        {
            return internalExternalTaskRepository.GetAvailableFHANumbers();
        }

        public AdditionalPropertyInfo GetPropertyInfoWithAEandWLM(string fhaNumber)
        {
            return projectInfoRepository.GetPropertyInfoWithAEandWLM(fhaNumber);
        }

        public bool UpdateInternalExternalTask(InternalExternalTaskModel internalExternalTask)
        {
            return internalExternalTaskRepository.UpdateInternalExternalTask(internalExternalTask);
        }
        public InternalExternalTaskModel GetNotCompletedTask(int pageTypeID, string fhaNumber)
        {
            return internalExternalTaskRepository.GetNotCompletedTask(pageTypeID, fhaNumber);
        }

        public UserViewModel GetUserInfoById(int ? userId)
        {
            if (userId == null)
                return new UserViewModel();
            return userInfoRepository.GetUserInfoById((int)userId);
        }

        /// <summary>
        /// Get user name by id from hcp authentication
        /// </summary>
        /// <param name="pUserId"></param>
        /// <returns></returns>
        public string GetUserNameById(int? pUserId)
        {
            string fName = string.Empty;
            string mName = string.Empty;
            string lName = string.Empty;
            string name = string.Empty;

            if (pUserId == null)
                return string.Empty;
            var objUserInfo =  userInfoRepository.GetUserInfoById((int)pUserId);
            fName = objUserInfo.FirstName;
            lName = objUserInfo.LastName;
            mName = !string.IsNullOrEmpty(objUserInfo.MiddleName) ? objUserInfo.MiddleName : " ";
            name = fName + mName + lName;
            return name;
        }


        public bool UpdateNoiTask(InternalExternalTaskModel model)
        {
            return internalExternalTaskRepository.UpdateNoiTask(model);
        }
    }
}
