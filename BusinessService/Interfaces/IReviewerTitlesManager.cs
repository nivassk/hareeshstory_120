﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces
{
    public interface IReviewerTitlesManager
    {
        List<ReviewerTitlesModel> GetReviewerTitles(int TitleTypeId);
        ReviewerTitlesModel GetReviewerTitle(int titleId);

    }
}
