﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
    public interface IProd_NextStageManager
    {
        Guid AddNextStage(Prod_NextStageModel model);
        List<string> GetFhasforNxtStage(int pagetypeid, int lenderId);
        bool UpdateTaskInstanceIdForNxtStage(Prod_NextStageModel model);
		Guid? GetNxtStageTaskid(Guid guidCurrentTaskinstanceid);
		Guid? GetCompletedStageTaskid(Guid guidCurrentTaskinstanceid);
	}
}
