﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;

namespace BusinessService
{
    public class ReviewFileCommentManager:IReviewFileCommentManager
    {
        private UnitOfWork unitOfWorkTask;
        private IReviewFileCommentRepository reviewFileCommentRepository;
        public ReviewFileCommentManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
            reviewFileCommentRepository = new ReviewFileCommentRepository(unitOfWorkTask);
        }

        public void SaveReviewFileComment(IList<ReviewFileCommentModel> reviewCommentList)
        {
            reviewFileCommentRepository.SaveReviewFileComment(reviewCommentList);
            unitOfWorkTask.Save();
        }
    }
}
