﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
   public class Prod_NoteManager : IProd_NoteManager
    {

       private IProd_NoteRepository prod_NoteRepository;
       private UnitOfWork unitOfWork;

       public Prod_NoteManager()
       {
           unitOfWork = new UnitOfWork(DBSource.Task);
           prod_NoteRepository = new Prod_NoteRepository(unitOfWork);
       }
        public int AddNote(Prod_NoteModel model)
        {
           return prod_NoteRepository.AddNote(model);
        }

        public List<Prod_NoteModel> GetAllTaskNotes(Guid taskInstanceId)
        {
            return prod_NoteRepository.GetAllTaskNotes(taskInstanceId);
        }

        public void DeleteNote(int noteId)
        {
            prod_NoteRepository.DeleteNote(noteId);
        }
    }
}
