﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--Update EmailTypeId=49
BEGIN
	IF EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] WHERE EmailTypeId=49)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]
		set MessageSubject='Firm Commitment Amendment Request – (Project Name [ProjectName]), (Project FHA Number [FHANumber])'
			,MessageBody='<p>A Firm Commitment Amendment Request was submitted on [SubmittedDate]. Please [check the Portal]  to review the request.<p>'
			,KeyWords='[ProjectName],[FHANumber],[SubmittedDate]'
		where EmailTypeId=49
	END
	ELSE
    BEGIN
		insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
		 select 37,49,'Firm Commitment Amendment Request – (Project Name [ProjectName]), (Project FHA Number [FHANumber])'
				,'<p>A Firm Commitment Amendment Request was submitted on [SubmittedDate]. Please [check the Portal]  to review the request.<p>'
				,'[ProjectName],[FHANumber],[SubmittedDate]','2018-11-09 15:42:05.350',NULL,'2018-11-09 15:42:05.350',NULL
		 where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where (EmailTypeId = 49 and MessageTemplateId = 37));

	END
END
--US 4852 : remove the option (9807 Termination Request Block 5 executed Block 5 Termination)
IF EXISTS (select * from HCP_Project_Action where ProjectActionID = 41)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_Project_Action]
		set Deleted_Ind = 1 
		where ProjectActionID = 41
END

--Change subject content for EmailTypeId=45
--update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
--set MessageSubject='Update DAP and the 232 Healthcare Portal''s Project Detail Screen for Firm Commitment Amendment Request [FirmCommitmentAmendment] – (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])'
-- where EmailTypeId=45

--Amendment WLM sign
insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 45,'WLMAMEND2','Amendment Email to Closer',0 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 45);

BEGIN
	IF  EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] WHERE EmailTypeId=45)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
		set MessageSubject='Update DAP and the 232 Healthcare Portal''s Project Detail Screen for Firm Commitment Amendment Request [FirmCommitmentAmendment] - (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])'
		 where EmailTypeId=45
	END
	ELSE
    BEGIN
		insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 34,45,'Update DAP and the 232 Healthcare Portal''s Project Detail Screen for Firm Commitment Amendment Request [FirmCommitmentAmendment] - (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])',
		'<p>A Firm Commitment Amendment [FirmCommitmentAmendment] was issued on [SignedDate]. Please update DAP and  Amendment tab of SharePoint in the Portal.</p>',
		'[FHANumber],[ProjectActionName],[FirmCommitmentAmendment],[SignedDate]',GETDATE(),1,GETDATE(),1 
		where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 34);
	END
END

--insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 34,45,'Update DAP and the 232 Healthcare Portal''s Project Detail Screen for Firm Commitment Amendment[FirmCommitmentAmendment] – (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])',
--'<p>A Firm Commitment Amendment [FirmCommitmentAmendment] was issued on [SignedDate]. Please update DAP and  Amendment tab of SharePoint in the Portal.</p>',
--'[FHANumber],[ProjectActionName],[FirmCommitmentAmendment],[SignedDate]',GETDATE(),1,GETDATE(),1 
--where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 34);




--update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
--set KeyWords='[FHANumber],[ProjectActionName],[FirmCommitmentAmendment]'
-- where MessageTemplateId=33


insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 44,'WLMAMEND1','Amendment Email to Lender',0 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 44);

BEGIN
	IF  EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] WHERE EmailTypeId=33)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
		set KeyWords='[FHANumber],[ProjectActionName],[FirmCommitmentAmendment]'
		where MessageTemplateId=33
	END
	ELSE
    BEGIN
		insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 33,44,'Firm Commitment Amendment[FirmCommitmentAmendment] – (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])',
		'<p>Firm Commitment Amendment  [FirmCommitmentAmendment] has been issued for this project and has been placed in the [Final Work Product section of the Portal]. </p>',
		'[FHANumber],[ProjectActionName],[FirmCommitmentAmendment]',GETDATE(),1,GETDATE(),1 
		where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 33);

	END
END

--insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 33,44,'Firm Commitment Amendment[FirmCommitmentAmendment] – (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])',
--'<p>Firm Commitment Amendment  [FirmCommitmentAmendment] has been issued for this project and has been placed in the [Final Work Product section of the Portal]. </p>',
--'[FHANumber],[ProjectActionName],[FirmCommitmentAmendment]',GETDATE(),1,GETDATE(),1 
--where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 33);

--update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
--set MessageSubject = 'FOR YOUR SIGNATURE: Firm Commitment Amendment – (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])'
--, MessageBody='A proposed Firm Commitment Amendment for this project has been reviewed and prepared by the assigned Closer. Please check the 232 Healthcare Portal to approve and sign the amendment.'
-- where MessageTemplateId=32

--Amendment WLM sign
insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 43,'WLMAMEND','Amendment Review and Authorization',0 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 43);


BEGIN
	IF EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] WHERE EmailTypeId=32)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
		set MessageSubject = 'FOR YOUR SIGNATURE: Firm Commitment Amendment – (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])'
		, MessageBody='A proposed Firm Commitment Amendment for this project has been reviewed and prepared by the assigned Closer. Please check the 232 Healthcare Portal to approve and sign the amendment.'
		 where MessageTemplateId=32
	END
	ELSE
    BEGIN
		insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 32,43,'FOR YOUR SIGNATURE: Firm Commitment Amendment – (Project Name [ProjectActionName]), (Project FHA Number [FHANumber])',
		'A proposed Firm Commitment Amendment for this project has been reviewed and prepared by the assigned Closer. Please check the 232 Healthcare Portal to approve and sign the amendment.',
		'[FHANumber],[ProjectActionName]',GETDATE(),1,GETDATE(),1 
		where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 32);

	END
END
--insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 32,43,'Amendment Authorization[#] – [ProjectActionName], [FHANumber]',
--'<p>WLM has to approve this amendment </p>',
--'[FHANumber],[ProjectActionName]',GETDATE(),1,GETDATE(),1 
--where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 32);

--Pam filter (Project stage)
--IF COL_LENGTH(' [$(DatabaseName)].dbo.[HUD_PamReportFilters]', 'ProjectStage') IS NULL
--BEGIN
--    ALTER TABLE [$(DatabaseName)].dbo.[HUD_PamReportFilters] ADD ProjectStage VARCHAR(100) NULL ;  
--END

--Amendment Deny template
insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 42,'DNYAMEND','Amendments Request Denied',0 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 42);

insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 49,'AMUWCLOSER','Amendment assignment Email to UWCLOSER',0
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 49);

 insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] 
 select 37,49,'Firm Commitment Amendment Request – [Project Name], [Project FHA Number]'
		,'<p>A Firm Commitment Amendment Request was submitted on [DATE]. Please [check the Portal]  to review the request.<p>'
		,'[Project Name], [Project FHA Number]','2018-11-09 15:42:05.350',NULL,'2018-11-09 15:42:05.350',NULL
 where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where (EmailTypeId = 49 and MessageTemplateId = 37));

 

insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 31,42,'Firm Commitment Amendment [#] – [ProjectActionName], [FHANumber]',
'<p>ORCF does not approve this amendment and it is withdrawn </p>',
'[FHANumber],[ProjectActionName]',GETDATE(),1,GETDATE(),1 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 31);

--US 4852 : remove the option (9807 Termination Request Block 5 executed Block 5 Termination)
IF EXISTS (select * from HCP_Project_Action where ProjectActionID = 41)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_Project_Action]
		set Deleted_Ind = 1 
		where ProjectActionID = 41
END

-- Prod Temp Task Re-Assignment templed 
insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 41,'PTSKRASSGN','Prod Temp Task Reassignemen',0 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 41);
insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 30,41,'Task Re-Assignment',
'<p>The task on [ProjectActionName] for [PropertyName], FHA Number - [FHANumber] has been assigned from [FromName] to [ToName] on [ReassignedDate]. [ToName] [ViewName] please check your My Reassigned Tasks for further details.</p>',
'[ProjectActionName],[PropertyName],[FHANumber],[FromName],[ToName],[ReassignedDate],[ViewName]',GETDATE(),1,GETDATE(),1 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 30);

--Amendment PageTypeId--
insert into [$(DatabaseName)].[dbo].[HCP_PageType] select 18,'AMENDMENTS',2,14 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_PageType]  where pagetypeid = 18);
insert into [$(DatabaseName)].[dbo].[HCP_DisclaimerText] select 6,'I acknowledge that I received the request for processing, and that I have reviewed it for completeness, accuracy and eligibility, in according to the Healthcare Mortgage Insurance Program Handbook No. 42321.(Production)'
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_DisclaimerText]  where DisclaimerTextId = 6);
insert into [$(DatabaseName)].[dbo].[PageType_DisclaimerText] select 6,18 
where not exists (select * from [$(DatabaseName)].[dbo].[PageType_DisclaimerText]  where PageTypeId = 18 and DisclaimerTextId=6);

--Task: 2793: R4R deny email type added--
insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 36,'R4RDeny','R4R Deny Notification',0
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 36);

--User Story 1884: DAP Reminder Email Type 
insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 48,'DAPEMAIL','Send DAP Reminder EMail',0
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 48);




--Task: 3294 Add/Update/delete records to HCP_Project_Action and PAMProjectActionTypes--
--INSERTS--
insert into HCP_Project_Action select '9807 Termination approval request for mortgage termination','9807 Termination approval request for mortgage termination',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =40)
--insert into HCP_Project_Action select 'Revised 9807 Termination request Block Five executed','Revised 9807 Termination request Block Five executed',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =41)
insert into HCP_Project_Action select 'Pending Approval- If the 9807 is incomplete and supporting documentation is missing','Pending Approval- If the 9807 is incomplete and supporting documentation is missing',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =42)
insert into HCP_Project_Action select 'Termination Letter','Termination Letter',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =43)
insert into HCP_Project_Action select 'TPA (Full) - Preliminary approval ','TPA (Full) - Preliminary approval ',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =44)
insert into HCP_Project_Action select 'TPA (Light) - Preliminary approval ','TPA (Light) - Preliminary approval ',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =45)
insert into HCP_Project_Action select 'TPA (Modified) - Preliminary approval ','TPA (Modified) - Preliminary approval ',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =46)
insert into HCP_Project_Action select 'CHOW - Preliminary approval ','CHOW - Preliminary approval ',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =47)
insert into HCP_Project_Action select 'CHOW - Final Approval','CHOW - Final Approval',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =48)
insert into HCP_Project_Action select 'Change In or Addition of Management Agent - Preliminary approval ','Change In or Addition of Management Agent - Preliminary approval ',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =49)
insert into HCP_Project_Action select 'AR Financing - Preliminary Approval','AR Financing - Preliminary Approval',NULL,NULL,Getdate(),-1,NULL,0,3 where not exists (select * from HCP_Project_Action where ProjectActionID =50)

insert into PAMProjectActionTypes select 28,'Termination Letter' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 28)
insert into PAMProjectActionTypes select 29,'TPA (Full) - Preliminary approval ' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 29)
insert into PAMProjectActionTypes select 30,'TPA (Light) - Preliminary approval ' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 30)
insert into PAMProjectActionTypes select 31,'TPA (Modified) - Preliminary approval ' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 31)
insert into PAMProjectActionTypes select 32,'CHOW - Preliminary approval ' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 32)
insert into PAMProjectActionTypes select 33,'CHOW - Final Approval' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 33)
insert into PAMProjectActionTypes select 34,'Change In or Addition of Management Agent - Preliminary approval ' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 34)
insert into PAMProjectActionTypes select 35,'AR Financing - Preliminary Approval' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 35)
insert into PAMProjectActionTypes select 40,'9807 Termination approval request for mortgage termination' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 40)
--insert into PAMProjectActionTypes select 41,'Revised 9807 Termination request Block Five executed' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 41)
insert into PAMProjectActionTypes select 42,'Pending Approval- If the 9807 is incomplete and supporting documentation is missing' where not exists (select * from PAMProjectActionTypes where PamProjectActionId = 42)

--UPDATES--
update HCP_Project_Action set ProjectActionName = 'TPA (Full) - Final Approval' where ProjectActionID =30
update HCP_Project_Action set ProjectActionName = 'TPA (Light) - Final Approval' where ProjectActionID =31
update HCP_Project_Action set ProjectActionName = 'TPA (Modified) - Final Approval' where ProjectActionID =32
update HCP_Project_Action set ProjectActionName = 'Change In or Addition of Management Agent - Final Approval ' where ProjectActionID =8
update HCP_Project_Action set ProjectActionName = 'AR Financing- Final Approval' where ProjectActionID =3

update PAMProjectActionTypes set Name ='TPA (Full) - Final Approval' where PamProjectActionId =6
update PAMProjectActionTypes set Name ='TPA (Light) - Final Approval' where PamProjectActionId =7
update PAMProjectActionTypes set Name ='TPA (Modified) - Final Approval' where PamProjectActionId =8
update PAMProjectActionTypes set Name ='Change In or Addition of Management Agent - Final Approval ' where PamProjectActionId =5
update PAMProjectActionTypes set Name ='AR Financing- Final Approval' where PamProjectActionId =3

--delete --
delete from HCP_Project_Action where  exists (select * from HCP_Project_Action where ProjectActionID = 41 and ProjectActionName like '%Revised%')
delete from  PAMProjectActionTypes where PamProjectActionId = 41  and Name like '%Revised%' 

--Task 3630: Add Production Indexes for Performance Issues--
IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_Lender_FHANumber_7_1698105090__K1_K2_K7' ) 
BEGIN  
CREATE NONCLUSTERED INDEX [_dta_index_Lender_FHANumber_7_1698105090__K1_K2_K7] ON [dbo].[Lender_FHANumber]
(
	[LenderID] ASC,
	[FHANumber] ASC,
	[FHA_EndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_HCP_Authentication_7_738101670__K5_1_2_3' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_HCP_Authentication_7_738101670__K5_1_2_3] ON [dbo].[HCP_Authentication]
(
	[UserName] ASC
)
INCLUDE ( 	[UserID],
	[LastName],
	[FirstName]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO


IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_HCP_Authentication_7_738101670__K5_K1' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_HCP_Authentication_7_738101670__K5_K1] ON [dbo].[HCP_Authentication]
(
	[UserName] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_HCP_Authentication_7_738101670__K5_K1_2_3' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_HCP_Authentication_7_738101670__K5_K1_2_3] ON [dbo].[HCP_Authentication]
(
	[UserName] ASC,
	[UserID] ASC
)
INCLUDE ( 	[LastName],
	[FirstName]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_OPAForm_7_811149935__K17_K1_2_8_16' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_OPAForm_7_811149935__K17_K1_2_8_16] ON [dbo].[OPAForm]
(
	[TaskInstanceId] ASC,
	[ProjectActionFormId] ASC
)
INCLUDE ( 	[FhaNumber],
	[ProjectActionTypeId],
	[AEComments]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_OPAForm_7_811149935__K2_3' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_OPAForm_7_811149935__K2_3] ON [dbo].[OPAForm]
(
	[FhaNumber] ASC
)
INCLUDE ( 	[PropertyName]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_OPAForm_7_811149935__K2_K8_K1_K11_3_17' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_OPAForm_7_811149935__K2_K8_K1_K11_3_17] ON [dbo].[OPAForm]
(
	[FhaNumber] ASC,
	[ProjectActionTypeId] ASC,
	[ProjectActionFormId] ASC,
	[CreatedBy] ASC
)
INCLUDE ( 	[PropertyName],
	[TaskInstanceId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_OPAForm_7_811149935__K8_K2_K1_K11_17' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_OPAForm_7_811149935__K8_K2_K1_K11_17] ON [dbo].[OPAForm]
(
	[ProjectActionTypeId] ASC,
	[FhaNumber] ASC,
	[ProjectActionFormId] ASC,
	[CreatedBy] ASC
)
INCLUDE ( 	[TaskInstanceId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_ProjectInfo_7_1586104691__K3_1_2_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31_32_' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_ProjectInfo_7_1586104691__K3_1_2_4_5_6_7_8_9_10_11_12_13_14_15_16_17_18_19_20_21_22_23_24_25_26_27_28_29_30_31_32_] ON [dbo].[ProjectInfo]
(
	[FHANumber] ASC
)
INCLUDE ( 	[ProjectInfoID],
	[PropertyID],
	[ProjectName],
	[Property_AddressID],
	[LenderID],
	[ServicerID],
	[ManagementID],
	[HUD_WorkLoad_Manager_ID],
	[HUD_Project_Manager_ID],
	[ModifiedOn],
	[ModifiedBy],
	[OnBehalfOfBy],
	[Primary_Loan_Code],
	[Initial_Endorsement_Date],
	[Final_Endorsement_Date],
	[Field_Office_Status_Name],
	[Soa_Code],
	[Soa_Numeric_Name],
	[Soa_Description_Text],
	[Troubled_Code],
	[Troubled_status_update_date],
	[Reac_Last_Inspection_Score],
	[Reac_Last_Inspection_Date],
	[Mddr_In_Default_Status_Name],
	[Is_Active_Dec_Case_Ind],
	[Original_Loan_Amount],
	[Original_Interest_Rate],
	[Amortized_Unpaid_Principal_Bal],
	[Is_Nursing_Home_Ind],
	[Is_Board_And_Care_Ind],
	[Is_Assisted_Living_Ind],
	[Deleted_Ind],
	[Lender_AddressID],
	[Lender_Mortgagee_Name],
	[Servicer_AddressID],
	[Servicing_Mortgagee_Name],
	[Owner_AddressID],
	[Owner_Company_Type],
	[Owner_Legal_Structure_Name],
	[Owner_Organization_Name],
	[Owner_Contact_AddressID],
	[Owner_Contact_Indv_Fullname],
	[Owner_Contact_Indv_Title],
	[Mgmt_Agent_AddressID],
	[Mgmt_Agent_Company_Type],
	[Mgmt_Agent_org_name],
	[Mgmt_Contact_AddressID],
	[Mgmt_Contact_FullName],
	[Mgmt_Contact_Indv_Title],
	[Portfolio_Number],
	[Portfolio_Name]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT * from sys.indexes where name ='_dta_index_ProjectInfo_7_1586104691__K3_K1' ) 
BEGIN 
CREATE NONCLUSTERED INDEX [_dta_index_ProjectInfo_7_1586104691__K3_K1] ON [dbo].[ProjectInfo]
(
	[FHANumber] ASC,
	[ProjectInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

GO


-- Firm Commitment App templet with out OGC information 
insert into [$(DatabaseName)].[dbo].[HCP_EmailType] select 50,'FIRMAP2','Firm Commitment App No OGC',0 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailType]  where EmailTypeId = 50);

insert into [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] select 38,50,'Firm Commitment Issued',
'<p>&nbsp;</p>
<p>Congratulations on your HUD Loan Committee approval.&nbsp;</p>
<p>An electronic version of the HUD-signed firm commitment is attached.&nbsp;</p>
<ol>
<li>Please sign and have the Borrower sign the firm commitment within 10 days of receipt and provide a soft copy back through the Healthcare Portal.&nbsp;&nbsp;Please keep the original copy and submit with your closing package to the assigned HUD Closer.<br />&nbsp;</li>
<li>If you need to update any of the contact information previously submitted in the application&rsquo;s Exhibit 1-6-Contacts, please include it with the submission to the Healthcare Portal returning the electronic copy of the fully executed firm commitment.</li>
</ol>
<p><strong><u>Extension and Amendment Requests</u></strong>:</p>
<p>&nbsp;</p>
<ul>
<li>All Extension and Amendment requests will be handled through the Healthcare Portal.</li>
<li>Please hold any substantive amendments for the assigned Closer.&nbsp;&nbsp;If an amendment is required prior to submission of the closing package, please submit the request through the Healthcare Portal.</li>
</ul>
<p><strong><u>Closing:</u></strong></p>
<p>&nbsp;</p>
<ol>
<li>Please send the&nbsp;<strong><u>complete</u></strong> legal closing package to the assigned HUD Attorney.&nbsp;&nbsp;We will provide additional details once that assignment is made.</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>After you have submitted your draft legal package and you are ready to submit a&nbsp;<strong><u>complete</u></strong> closing package to the ORCF closer, please email&nbsp;<a href="mailto:ORCFCloser@hud.gov">ORCFCloser@hud.gov</a>, letting HUD know when you mailed your legal closing package to the HUD Attorney.&nbsp; ORCF will send an email when a HUD Closer is assigned and provide you further information about the next steps toward closing.&nbsp;&nbsp;Submit the ORCF Closer Package through the Healthcare Portal.</li>
</ol>
<p><em>When submitting the ORCF Closer assignment request and the legal closing package, please include in the email to ORCF and the cover letter to the HUD Attorney, any hard&nbsp;</em><em>(known/existing)</em><em>&nbsp;deadlines that would impact the closing date of this project.&nbsp; This information will assist HUD in workload management.&nbsp; The closing date will be set by the HUD Attorney after consultation with the ORCF Closer, but HUD will not close a transaction until all program and legal requirements have been fully satisfied.</em></p>
<p>&nbsp;</p>
<ol>
<li><u>To expedite the closing process</u>, it is imperative that only COMPLETE, correct, and tabbed legal and closer packages are submitted to ORCF and OGC.</li>
<li>Projects with&nbsp;<strong>critical repairs</strong>, packages must include clear photos, invoices, and the signed and dated Owner&rsquo;s Certification.</li>
<li>All packages must also include a&nbsp;<strong>draft closing statement</strong>.</li>
<li><strong>Litigation searches</strong>should be run and analyzed by the Lender&nbsp;<em><u>no earlier than 30 days before closing and no later than 5 business days before closing.</u></em>&nbsp;</li>
<li>If packages are incomplete or substantially incorrect, HUD reserves the right to place the project back in the closing queue for assignment only when a corrected package is resubmitted</li>
</ol>
<p>&nbsp;</p>
<ol>
<li>As a reminder, legal documents are intended to apply to relevant transactions without editing.&nbsp; If any changes to legal documents (e.g. Regulatory Agreements, Lease Addenda, etc.) are proposed, please do not forget to submit the suggested changes to&nbsp;<strong>BOTH</strong>OGC and ORCF via a red-line document with comments or justifications for the change.&nbsp;&nbsp;<strong>Any proposed document change will need to be approved by OGC and OHP&nbsp;<u>prior</u>&nbsp;to closing</strong><strong>.</strong></li>
</ol>
<p>&nbsp;</p>
<ol>
<li><strong>Closing Date:</strong>&nbsp;&nbsp;When requesting a closing date, please remember your timetable for closing should include a minimum of three business days for the documents signed by HUD to be sent to the appropriate parties.&nbsp;&nbsp;Your timetable for closing should also factor in the number of days needed for pre-recording.&nbsp; We strongly suggest that you do not lock your rate without confirming that this closing is moving forward.&nbsp; Locking your rate prior to such confirmation will be at your own risk.&nbsp;A closing cannot take place until all ORCF and OGC requirements are fully satisfied.</li>
</ol>
<p>&nbsp;</p>
<p>For your convenience, please find the following tools:&nbsp;</p>
<p>[Include the below for 223(a)(7) projects]</p>
<ol>
<li>a)<u><a href="http://portal.hud.gov/hudportal/documents/huddoc?id=OGC_Closing_Checklist-a7.docx">HUD Attorney Checklist-223(a)(7)</a></u></li>
<li>b)<a href="http://portal.hud.gov/hudportal/documents/huddoc?id=Closer_Checklist-a7.docx">HUD Closer Checklist-223(a)(7)</a></li>
<li>c)<a href="http://portal.hud.gov/hudportal/documents/huddoc?id=Special_Condi_Mat-a7.docx">Special Conditions Matrix</a><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
</ol>
<p>[Include the below for 223(f) projects]</p>
<ol>
<li>a)<a href="http://portal.hud.gov/hudportal/documents/huddoc?id=OGC_Closing_Checklist-f.docx">HUD Attorney Checklist-223(f)</a></li>
<li>b)<a href="http://portal.hud.gov/hudportal/documents/huddoc?id=Closer_Checklist-f.docx">HUD Closer Checklist-223(f)</a></li>
<li>c)&nbsp;<a href="http://portal.hud.gov/hudportal/documents/huddoc?id=Special_Condi_Mat-f.docx">Special Conditions Matrix</a></li>
</ol>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p>Thank you for all of your time and assistance on this project.&nbsp; I look forward to working with you again.</p>
<p><strong>[Underwriter]</strong></p>',
'[Underwriter]',GETDATE(),1,GETDATE(),1 
where not exists (select * from [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 38);

--IF EXISTS(SELECT
--    COLUMN_NAME, TABLE_NAME
--    FROM INFORMATION_SCHEMA.COLUMNS 
--    WHERE TABLE_NAME = 'NonCriticalRepairsRequests' and COLUMN_NAME = 'ClearTitleFileName')
--BEGIN
--EXEC sp_RENAME 'NonCriticalRepairsRequests.ClearTitleFileName', 'FinalInspectionReport', 'COLUMN'
--END
--GO


--UserStory 1928
insert into [$(DatabaseName)].
[dbo].[HCP_EmailType] select 47,'LENSUBAMND','Lender Submit Amendment Request',0 
where not exists (select * from [$(DatabaseName)].
[dbo].[HCP_EmailType]  where EmailTypeId = 47);

BEGIN
	IF EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[HCP_EmailType] WHERE EmailTypeId=44)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_EmailType]
		set EmailTypeDescription = 'Firm Commitment Amendment'
		where EmailTypeId=44
	END
	ELSE
	IF EXISTS (SELECT * FROM [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages] WHERE EmailTypeId=47)
	BEGIN
		update [$(DatabaseName)].[dbo].[HCP_EmailTemplateMessages]
		set MessageSubject= 'Firm Commitment Amendment Request Received - (Project Name [ProjectName]) , (Project FHA Number [FHANumber])'
			,KeyWords='[ProjectName],[FHANumber]'
		where EmailTypeId=47
	END
	ELSE
    BEGIN
		insert into [$(DatabaseName)].
		[dbo].[HCP_EmailTemplateMessages] select 36,47,'Firm Commitment Amendment Request Received - (Project Name [ProjectName]) , (Project FHA Number [FHANumber])',
		'<p>A Firm Commitment Amendment Request has been received, and will be reviewed by the assigned Closer (once assigned, if no assignment has yet been made). Once the Amendment has been approved and signed, you will be notified through the Portal.</p>',
		'[ProjectName],[FHANumber]',GETDATE(),1,GETDATE(),1 
		where not exists (select * from [$(DatabaseName)].
		[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 36);		
	END
END

--insert into [$(DatabaseName)].
--[dbo].[HCP_EmailTemplateMessages] select 36,47,'Firm Commitment Amendment Request Received – [Project Name], [Project FHA Number]',
--'<p>A Firm Commitment Amendment Request has been received, and will be reviewed by the assigned Closer (once assigned, if no assignment has yet been made). Once the Amendment has been approved and signed, you will be notified through the Portal.</p>',
--'[Project Name], [Project FHA Number]',GETDATE(),1,GETDATE(),1 
--where not exists (select * from [$(DatabaseName)].
--[dbo].[HCP_EmailTemplateMessages]  where MessageTemplateId = 36);


--Created view for the SSIS package Task - 4582
--IF NOT EXISTS(select * FROM sys.views where name = 'vwNonProdLenderInfo')
--BEGIN
--	exec('create view [dbo].vwNonProdLenderInfo 
--	as 
--	select li.* from LenderInfo li
--	where li.LenderID not in (select  LenderId from prodlender_lookup)')
--END


-- Bug 4661
insert into [$(DatabaseName)].[dbo].HUD_Project_Manager select 'DORA D DAVENPORT',1481452,getdate(),-1,null,null
where not exists (select * from [$(DatabaseName)].[dbo].HUD_Project_Manager  where HUD_Project_Manager_Name = 'DORA D DAVENPORT')

insert into [$(DatabaseName)].[dbo].User_WLM_PM select 5887,22,(select HUD_Project_Manager_Id from [$(DatabaseName)].[dbo].HUD_Project_Manager  where HUD_Project_Manager_Name = 'DORA D DAVENPORT'),getdate(),-1,null,null
where not exists (select * from [$(DatabaseName)].[dbo].User_WLM_PM  where userid = 5887)

--Task 4702
update webpages_UsersInRoles set RoleId = 6
where userid in (6060,6066,5885) and RoleId != 6

--Task 4770

update webpages_UsersInRoles set RoleId = 1016
where userid in (6221,6218) and RoleId != 1016