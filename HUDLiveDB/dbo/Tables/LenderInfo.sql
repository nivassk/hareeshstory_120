﻿
CREATE TABLE [dbo].[LenderInfo](
	[LenderID] [int] NOT NULL,
	[Lender_Name] [nvarchar](100) NOT NULL,
	[AddressID] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_LenderInfo] PRIMARY KEY CLUSTERED 
(
	[LenderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

