﻿
CREATE TABLE [dbo].[Comment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [int] NULL,
	[FHANumber] [nvarchar](15) NULL,
	[Subject] [nvarchar](256) NOT NULL,
	[Comments] [xml] NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[LDI_ID] [int] NOT NULL,
 CONSTRAINT [PK_CommentId] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

