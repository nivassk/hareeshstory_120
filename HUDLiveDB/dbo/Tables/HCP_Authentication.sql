﻿
CREATE TABLE [dbo].[HCP_Authentication](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[UserMInitial] [varchar](50) NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[AddressID] [int] NULL,
	[Deleted_Ind] [bit] NULL,
	[PreferredTimeZone] [nvarchar](50) NOT NULL,
	[PasswordHist] [nvarchar](max) NULL,
	[IsRegisterComplete] [bit] NULL,
	[TitleId] [int] NULL,
 CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[HCP_Authentication]  WITH CHECK ADD  CONSTRAINT [FK_HCP_Authentication_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO

ALTER TABLE [dbo].[HCP_Authentication] CHECK CONSTRAINT [FK_HCP_Authentication_Address]
GO

