﻿
CREATE TABLE [dbo].[User_Lender](
	[User_Lender_ID] [int] IDENTITY(1,1) NOT NULL,
	[User_ID] [int] NOT NULL,
	[Lender_ID] [int] NULL,
	[ServicerID] [int] NULL,
	[DateInserted] [datetime] NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[FHANumber] [nvarchar](15) NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_User_Lender] PRIMARY KEY CLUSTERED 
(
	[User_Lender_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[User_Lender]  WITH CHECK ADD  CONSTRAINT [FK_User_Lender_HCP_Authentication] FOREIGN KEY([User_ID])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[User_Lender] CHECK CONSTRAINT [FK_User_Lender_HCP_Authentication]
GO

ALTER TABLE [dbo].[User_Lender]  WITH CHECK ADD  CONSTRAINT [FK_User_Lender_LenderInfo] FOREIGN KEY([Lender_ID])
REFERENCES [dbo].[LenderInfo] ([LenderID])
GO

ALTER TABLE [dbo].[User_Lender] CHECK CONSTRAINT [FK_User_Lender_LenderInfo]
GO

ALTER TABLE [dbo].[User_Lender]  WITH CHECK ADD  CONSTRAINT [FK_User_Lender_ServicerInfo] FOREIGN KEY([ServicerID])
REFERENCES [dbo].[ServicerInfo] ([ServicerID])
GO

ALTER TABLE [dbo].[User_Lender] CHECK CONSTRAINT [FK_User_Lender_ServicerInfo]
GO

