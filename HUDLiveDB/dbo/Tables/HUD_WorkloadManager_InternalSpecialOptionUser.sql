﻿
CREATE TABLE [dbo].[HUD_WorkloadManager_InternalSpecialOptionUser](
	[HUD_WorkloadManagerId] [int] NOT NULL,
	[InternalSpecialOptionUserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[HUD_WorkloadManagerId] ASC,
	[InternalSpecialOptionUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HUD_WorkloadManager_InternalSpecialOptionUser]  WITH CHECK ADD FOREIGN KEY([HUD_WorkloadManagerId])
REFERENCES [dbo].[HUD_WorkLoad_Manager] ([HUD_WorkLoad_Manager_ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[HUD_WorkloadManager_InternalSpecialOptionUser]  WITH CHECK ADD FOREIGN KEY([InternalSpecialOptionUserId])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
ON DELETE CASCADE
GO

