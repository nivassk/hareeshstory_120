﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetUploadUserByLdiID]
(
@LdiID INT
)
AS

SELECT 
	u.[UserID],
	u.[LastName],
	u.[FirstName],
	u.[UserMInitial],
	u.[UserName],
	u.[ModifiedOn],
	u.[ModifiedBy],
	u.[OnBehalfOfBy],
	u.[AddressID],
	u.[Deleted_Ind],
	u.[PreferredTimeZone],
	u.[PasswordHist],
	u.[IsRegisterComplete]
FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] li
INNER JOIN [$(DatabaseName)].[dbo].[HCP_Authentication] u
ON li.UserID = u.UserID
WHERE LI.LDI_ID = @LdiID

GO

