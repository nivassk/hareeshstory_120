﻿
-- =============================================
-- Create date: 10/4/2016
-- Description:Get Disclaimet Language for Page type
-- =============================================
CREATE PROCEDURE[dbo].[usp_HCP_GetDisclaimerMsgByPageType]
	(
		@PageTypeName nvarchar(50)
	)
AS
BEGIN


	select [DisclaimerTextDescription] 
	from [dbo].[HCP_DisclaimerText] dt
	inner join [dbo].[PageType_DisclaimerText] pd
	 on pd.DisclaimerTextId = dt.DisclaimerTextId
	inner join [dbo].[HCP_PageType] pt
	on pt.PageTypeId = pd.PageTypeId
	where pt.PageTypeDescription= @PageTypeName

END

