﻿
create PROCEDURE [dbo].[usp_HCP_Prod_GetOPAByChildTaskId]
(
@TaskInstanceId uniqueidentifier
)	
AS
BEGIN
	 
	SET NOCOUNT ON;	
	SELECT OP.* 
	FROM [$(TaskDB)].dbo.ParentChildTask PT
	inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskXrefid = pt.ParentTaskInstanceId
	JOIN OPAForm OP ON xref.TaskInstanceId = OP.TaskInstanceId

	WHERE PT.ChildTaskInstanceId = @TaskInstanceId
END

GO

