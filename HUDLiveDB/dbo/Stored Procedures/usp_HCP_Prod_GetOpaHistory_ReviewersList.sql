﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetOpaHistory_ReviewersList]
(
@FileId int,
@ViewId int,
@UserId int
)
AS
/*
Date			Changed by					Reason
====			===========					======	
03/28/2018		Rashmi/Venkatesh/Suresh		Filter the results by production roles  

*/
BEGIN


--DECLARE @LenderId int 
-- select @LenderId = count (au.UserID) from [dbo].[HCP_Authentication] au
--  inner join [dbo].[webpages_UsersInRoles] wu on wu.UserId = au.UserID
--  inner join [dbo].[webpages_Roles] r on r.RoleId = wu.RoleId
--  where r.RoleName in ('Lender','LenderAccountManager','LenderAccountRepresentative','BackupAccountManager')
--  and au.UserID = @UserId


select distinct 	
		(ato.FirstName +' ' +aTo.LastName) as Name,
		rTo.RoleName +'('+rv.[ViewName] +')' as  userRole,
	rf.[Status] ,
	   CAST(
				CASE 
					WHEN rf.[Status] = 4
						THEN 1 
					ELSE 0
				END as bit) as isReqAddInfo,
				CAST(
				CASE 
					WHEN rf.[Status] = 5
						THEN 1 
					ELSE 0
				END as bit) as isApprove,
				
				raiComments=CASE 
					WHEN rf.[Status] = 4
						THEN rfcFinal.Comment  
					WHEN rf.[Status] = 5
						THEN COALESCE(rfcFinal.Comment  , '') 
					ELSE ''
					end,

					rf.ModifiedOn  as submitDate
from [$(TaskDB)].dbo.TaskFile tfparent
inner join [$(TaskDB)].dbo.Task t on t.TaskInstanceId = tfparent.TaskInstanceId and t.SequenceId = 0
inner join [$(TaskDB)].dbo.Prod_TaskXref xref on xref.TaskInstanceId = t.TaskInstanceId
inner join HCP_Authentication aTo on aTo.UserID = xref.AssignedTo
left join [$(TaskDB)].[dbo].[Prod_View] rv on rv.[ViewId] = xref.ViewId
inner join webpages_UsersInRoles lTo on lTo.UserId = aTo.userid
inner join webpages_Roles rTo on rTo.roleid = lTo.roleid
left join [$(TaskDB)].dbo.ReviewFileStatus rf on tfparent.TaskFileId = rf.TaskFileId and xref.AssignedTo = rf.ReviewerUserId and xref.ViewId = rf.ReviewerProdViewId
left join [$(TaskDB)].dbo.ReviewStatusList rsl on rsl.StatusId = rf.[Status]
left join  (select rfc.FileTaskId,rfc.comment,rfc.CreatedBy,rfc.ReviewerProdViewId from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId,CreatedBy, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId , [CreatedBy] )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate and  rfc.CreatedBy = rfcl.CreatedBy)rfcFinal on rfcFinal.FileTaskId = tfparent.TaskFileId and  xref.AssignedTo = rfcFinal.CreatedBy and xref.ViewId = rfcFinal.ReviewerProdViewId
left join  (select rfc.FileTaskId,rfc.comment from [$(TaskDB)].dbo.ReviewFileComment rfc
			inner join 
			(select FileTaskId, max(CreatedOn) as MaxDate 
			 from [$(TaskDB)].dbo.[ReviewFileComment]  group by FileTaskId  )rfcl on rfc.FileTaskId = rfcl.FileTaskId and rfc.CreatedOn = rfcl.MaxDate )rfcHistory on rfcHistory.FileTaskId = tfparent.TaskFileId

   -- where   (tfparent.FileId =  @FileId and @LenderId > 0) or 
			--(tfparent.FileId =  @FileId and xref.AssignedTo != @UserId and xref.ViewId != @ViewId and @LenderId = 0 )

			where   tfparent.FileId =  @FileId and rTo.RoleName in ('ProductionUser', 'ProductionWlm', 'Reviewer')

end

GO

