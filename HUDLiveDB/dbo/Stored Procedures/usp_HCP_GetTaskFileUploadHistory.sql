﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetTaskFileUploadHistory]
(
@TaskFileId int
)
AS

BEGIN

select distinct 
               (A.FirstName + ' ' + A.LastName) as name,
              CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM'
                     WHEN r.RoleName = 'OperatorAccountRepresentative' then 'OAR' end as userRole,
          tf.TaskFileId as taskFileId,
          tf.FileId as fileId ,
          tf.[FileName] as [fileName],
           tf.fileSize as fileSize,
          (tf.UploadTime) as uploadDate
          
from  [$(TaskDB)].dbo.TaskFile tf
left join [$(TaskDB)].dbo.TaskFileHistory tfh on tf.TaskFileId = tfh.ChildTaskFileId
inner join HCP_Authentication a on a.UserID = tf.CreatedBy
inner join webpages_UsersInRoles l on l.UserId = a.userid
inner join webpages_Roles r on r.roleid = l.roleid
where [ParentTaskFileId] = (select t.TaskFileId from [$(TaskDB)].dbo.[TaskFile] t where t.FileId= @TaskFileId)
       or tf.FileId = @TaskFileId


end


GO

