﻿
create PROCEDURE [dbo].[usp_HCP_Prod_GetOPAByTaskXrefId]
(
@TaskInstanceId uniqueidentifier
)	
AS
BEGIN
	 
	SET NOCOUNT ON;	
	SELECT OP.* 
	FROM OPAForm OP 
	inner join hcp_task_staging.dbo.Prod_TaskXref xref on xref.TaskXrefid =  OP.TaskInstanceId
	WHERE xref.TaskXrefid = @TaskInstanceId 

END

GO

