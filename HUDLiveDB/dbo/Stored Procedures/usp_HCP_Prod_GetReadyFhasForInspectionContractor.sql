﻿
Create PROCEDURE [dbo].[usp_HCP_Prod_GetReadyFhasForInspectionContractor]
  AS
   
    SELECT fhaRequest.FHANumber,loanType.ProjectTypeName as FHADescription
	FROM Prod_FHANumberRequest fhaRequest
	join Prod_ProjectType loanType ON fhaRequest.ProjectTypeId = loanType.ProjectTypeId
    WHERE IsReadyForApplication='True' and loanType.ProjectTypeName in ('Construction 241(a)','Construction NC','Construction SR') and fhaRequest.FHANumber is not null 
	      and fhaRequest.FHANumber not in( SELECT FHANumber FROM User_Lender
                                WHERE FHANumber is not null AND 
								User_ID in (select userRoles.UserId FROM webPages_Roles roles
                                                  join webpages_UsersInRoles userRoles on roles.RoleId = userRoles.RoleId
                                                  WHERE RoleName = 'InspectionContractor'
												)
							   )

GO

