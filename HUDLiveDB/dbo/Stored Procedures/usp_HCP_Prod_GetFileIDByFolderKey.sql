﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetFileIDByFolderKey]
(
@FolderKey int
)
AS

BEGIN
select tf.FileId from [$(TaskDB)].[dbo].[TaskFile_FolderMapping] tfm join [$(TaskDB)].[dbo].[TaskFile] tf on tfm.TaskFileId= tf.TaskFileId 
where tfm.FolderKey=@FolderKey 
		
end

GO

