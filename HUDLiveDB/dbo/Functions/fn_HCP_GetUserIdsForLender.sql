﻿CREATE FUNCTION [dbo].[fn_HCP_GetUserIdsForLender]
(
	@FHANumber nvarchar(15),
	@LenderId int,
	@UserRole nvarchar(30)
)
RETURNS TABLE
AS
RETURN
(
	SELECT DISTINCT ul.User_ID FROM user_lender ul, webpages_usersinroles wu, webpages_Roles wr
	WHERE (ul.fhanumber = @FHANumber OR ul.Lender_ID = @LenderId)
	AND ul.User_ID = wu.UserId AND wr.RoleName = @UserRole AND wr.RoleId = wu.RoleId
	AND (ul.Deleted_Ind = 0 OR ul.Deleted_Ind IS NULL)
);