﻿
CREATE FUNCTION [dbo].[fn_HCP_GetQuarterForPreviousYear]
(
	@MonthInPeriod INT,
	@DiffValue INT
	
)
RETURNS VARCHAR(2)
AS
BEGIN
	DECLARE @QuarterPart VARCHAR(2)
	IF @DiffValue = 9
	BEGIN
		SET @QuarterPart =
		CASE @MonthInPeriod 
			 WHEN 3 THEN 'Q2'
			 WHEN 6 THEN 'Q3'
			 WHEN 9 THEN 'Q4'
			 WHEN 12 THEN 'Q1'
			 ELSE '??'
		END   
	END
	ELSE IF @DiffValue = 6
	BEGIN
		SET @QuarterPart =
		CASE @MonthInPeriod 
			 WHEN 3 THEN 'Q3'
			 WHEN 6 THEN 'Q4'
			 WHEN 9 THEN 'Q1'
			 WHEN 12 THEN 'Q2'
			 ELSE '??'
		END   
	END
	ELSE IF @DiffValue = 3
	BEGIN
		SET @QuarterPart =
		CASE @MonthInPeriod 
			 WHEN 3 THEN 'Q4'
			 WHEN 6 THEN 'Q1'
			 WHEN 9 THEN 'Q2'
			 WHEN 12 THEN 'Q3'
			 ELSE '??'
		END   
	END
    RETURN @QuarterPart
END

