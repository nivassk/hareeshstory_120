﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces;

namespace Repository.ManagementReport
{
    public class MissingProjectReportRepository : BaseRepository<Lender_DataUpload_Live>, IMissingProjectReportRepository
    {
        private ReportModel _missingProjectReportModel;
        private ILenderFhaRepository _lenderFhaRepository;
        private IProjectInfoRepository _projectInfoRepository;

        public MissingProjectReportRepository()

            : base(new UnitOfWork(DBSource.Live))
        {
            _lenderFhaRepository = new LenderFhaRepository();
            _projectInfoRepository = new ProjectInfoRepository();
            _missingProjectReportModel = new ReportModel(ReportType.MissingProjectReport);
        }

        public ReportModel GetMissingProjectReportForSuperUser(int year)
        {

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_MissingProject_ManagementReport, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_Get_MissingProject_ManagementReport_HighLevel_Result>(
                    "usp_HCP_Get_MissingProject_ManagementReport_HighLevel",
                    new {Year = year}).ToList();
            var reportModel = new ReportModel(ReportType.MissingProjectReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
                Mapper.Map<IEnumerable<usp_HCP_Get_MissingProject_ManagementReport_HighLevel_Result>, IEnumerable<ReportGridModel>>(
                    results).ToList();
            foreach (var item in reportModel.ReportGridModelList)
            {
                item.ReportYear = (ReportYear)EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));
            }
            reportModel.ReportHeaderModel.ReportYear =
                (ReportYear) EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));
            return reportModel;
        }

        public ReportModel GetMissingProjectReportForSuperUserWithWLM(string userName, string userType, int year)
        {

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_MissingProject_ManagementReport, please pass in correct context in unit of work.");
           
            var results =
                context.Database.SqlQuerySimple<usp_HCP_Get_MissingProject_ManagementReport_Result>(
                    "usp_HCP_Get_MissingProject_ManagementReport",
                    new {UserType = userType, Username = userName, Year = year}).ToList();
            var reportModel = new ReportModel(ReportType.MissingProjectReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
               Mapper.Map<IEnumerable<usp_HCP_Get_MissingProject_ManagementReport_Result>, IEnumerable<ReportGridModel>>(
                   results).ToList();
            var addressModelList =
                Mapper.Map<IEnumerable<usp_HCP_Get_MissingProject_ManagementReport_Result>, IEnumerable<AddressModel>>(
                    results).ToList();
            int index = 0;
            foreach (var item in reportModel.ReportGridModelList)
            {
                Mapper.Map(addressModelList[index], item.HudWorkloadManager.AddressModel);
                item.ReportYear = (ReportYear)EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));
                index++;
            }
            reportModel.ReportHeaderModel.ReportYear =
                (ReportYear)EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));   
            return reportModel;
        }

        public ReportModel GetMissingProjectReportForWorkloadManager(string userName, string userType, int year)
        {

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_MissingProject_ManagementReport, please pass in correct context in unit of work.");
           
            var results =
                context.Database.SqlQuerySimple<usp_HCP_Get_MissingProject_ManagementReport_Result>(
                    "usp_HCP_Get_MissingProject_ManagementReport",
                    new {UserType = userType, Username = userName, Year = year}).ToList();
            var reportModel = new ReportModel(ReportType.MissingProjectReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
                Mapper.Map<IEnumerable<usp_HCP_Get_MissingProject_ManagementReport_Result>, IEnumerable<ReportGridModel>>(
                    results).ToList();
            var addressModelList =
                Mapper.Map<IEnumerable<usp_HCP_Get_MissingProject_ManagementReport_Result>, IEnumerable<AddressModel>>(
                    results).ToList();
            int index = 0;
            foreach (var item in reportModel.ReportGridModelList)
            {
                item.ReportYear = (ReportYear)EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));
                Mapper.Map(addressModelList[index], item.HudWorkloadManager.AddressModel);
                index++;
            }
            if (reportModel.ReportGridModelList.Count > 0)
            {
                var reportHeaderModel = Mapper.Map<ReportGridModel, ReportHeaderModel>(reportModel.ReportGridModelList[0]);
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.ReportType = ReportType.MissingProjectReport;
            }
            reportModel.ReportHeaderModel.ReportYear =
                (ReportYear)EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));
            return reportModel;
        } 

        public ReportModel GetMissingProjectReportForAccountExecutive(string userName, string userType, int year)
        {

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_MissingProject_ManagementReport, please pass in correct context in unit of work.");

            var results = context.Database.SqlQuerySimple<usp_HCP_Get_MissingProject_ManagementReport_Result>(
                "usp_HCP_Get_MissingProject_ManagementReport",
                new {UserType = userType, Username = userName, Year = year}).ToList();
            var reportModel = new ReportModel(ReportType.MissingProjectReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
               Mapper.Map<IEnumerable<usp_HCP_Get_MissingProject_ManagementReport_Result>, IEnumerable<ReportGridModel>>(
                   results).ToList();
            var addressModelList =
                Mapper.Map<IEnumerable<usp_HCP_Get_MissingProject_ManagementReport_Result>, IEnumerable<AddressModel>>(
                    results).ToList();
            int index = 0;
            foreach (var item in reportModel.ReportGridModelList)
            {
                item.ReportYear = (ReportYear)EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));
                Mapper.Map(addressModelList[index], item.HudAccountExecutive.AddressModel);
                index++;
            }
            if (reportModel.ReportGridModelList.Count > 0)
            {
                var reportHeaderModel =
                    Mapper.Map<ReportGridModel, ReportHeaderModel>(reportModel.ReportGridModelList[0]);
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.ReportType = ReportType.MissingProjectReport;
            }
            else
            {
                var reportHeaderModel = new ReportHeaderModel();
                var result = context.Database.SqlQuerySimple<usp_HCP_Get_WLM_AE_Details_Result>("usp_HCP_Get_WLM_AE_Details",
                    new { Username = userName});
                reportHeaderModel = Mapper.Map<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>(result.First());
                reportModel.ReportHeaderModel = reportHeaderModel;
            }
            reportModel.ReportHeaderModel.ReportYear =
                (ReportYear)EnumType.GetEnumFromDescription(year.ToString(CultureInfo.InvariantCulture), typeof(ReportYear));
            return reportModel;
        }
    }
}
