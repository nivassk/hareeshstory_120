﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.Repository
{
    //public abstract class BaseRepository<TEntity> : IRepository<TEntity>  where TEntity : class, new()
    public abstract class BaseRepository<TEntity> where TEntity : class, new()
    {
        private DbContext _context;
        private DbSet<TEntity> dbSet;
        private IQueryable<TEntity> dbQueryRoot;
        private UnitOfWork _unitOfWork;

        //public BaseRepository(IUnitOfWork unitOfWork)
        public BaseRepository(UnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this._context = unitOfWork.Context;
            _context.Database.CommandTimeout = 600;
            this.dbSet = _context.Set<TEntity>();
            this.dbQueryRoot = dbSet.AsQueryable();
        }

        //protected IUnitOfWork UnitOfWork
        protected UnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        protected DbContext Context
        {
            get { return _context; }
        }

        protected IQueryable<TEntity> DbQueryRoot
        {
            get { return dbQueryRoot; }
        }

        /// <summary>
        /// entity framework monitors context in memory for changes
        /// it does not aware of changes from database
        /// needs to reload to sync with database values in some cases
        /// </summary>
        /// <param name="entity"></param>
        protected TEntity GetByIDFresh(int id)
        {
            var entity = GetByIDFast(id);
            _context.Entry(entity).Reload();
            return entity;
        }

        protected virtual IEnumerable<TEntity> GetAll()
        {
            //IQueryable<TEntity> query = dbSet;            
            //return query;
            return dbSet;
        }

        /// <summary>
        /// includeProperties are for eager loading child entities, GetById does not support loading child entity
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        protected virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate, string includeProperties = "")
        {
            var query = dbQueryRoot.Where(predicate);
            if(!string.IsNullOrEmpty(includeProperties))
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            return query;
        }

        protected virtual PaginateSortModel<TEntity> FindWithPageSort(Expression<Func<TEntity, bool>> predicate,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, string includeProperties = "")
        {
            //var query = predicate == null ? GetAll() : Find(predicate);
            var query = Find(predicate, includeProperties);
            return query.SortAndPaginate(strSortBy, sortOrder, pageSize, pageNum);
        }

        /// <summary>
        /// Use the DbSet.SqlQuery method for queries that return entity types. The returned objects must be of the type 
        /// expected by the DbSet object, and they are automatically tracked by the database context unless you turn tracking off. 
        /// </summary>
        /// <param name="query">plain sql query with e.g. SELECT * FROM Upload WHERE PropertyID = @p0</param>
        /// <param name="parameters">unitOfWork.CourseRepository.GetWithRawSql(query, id)</param>
        /// <returns></returns>
        protected virtual IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters)
        {
            return dbSet.SqlQuery(query, parameters).ToList();
        }

        protected virtual TEntity GetByID(int id)
        { 
            return dbSet.Find(id);
        }

        /// <summary>
        /// dbSet.Find(id) is has performance hit as it calls DetectChanges in context
        /// this is a quicker version
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected virtual TEntity GetByIDFast(int id)
        {
            try
            {
                Context.Configuration.AutoDetectChangesEnabled = false;
                return GetByID(id);
            }
            finally
            {
                Context.Configuration.AutoDetectChangesEnabled = true;
            }
        }

        /// <summary>
        /// for eager load to save db IO, use Find(p => p.id = id, includeProperties)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="includeChildRefName"></param>
        /// <returns></returns>
        protected virtual TEntity GetByIdWithLazyLoadRef(int id, string includeChildRefName)
        {
            var entity = GetByIDFast(id);
            Context.Entry(entity).Reference(includeChildRefName).Load();
            return entity;
        }

        protected virtual TEntity GetByIdWithLazyLoadCol(int id, string includeChildrenColName)
        {
            var entity = GetByIDFast(id);
            Context.Entry(entity).Collection(includeChildrenColName).Load();
            return entity;
        }

        protected virtual void AttachNew(TEntity entity)
        {
            dbSet.Add(entity);
        }

        protected virtual void InsertNew(TEntity entity)
        {
            AttachNew(entity);
        }

        protected virtual void Delete(int id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        protected virtual void Delete(TEntity entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == System.Data.Entity.EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        protected virtual void Update(TEntity entityToUpdate)
        {
            Context.Configuration.AutoDetectChangesEnabled = false;
            dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = System.Data.Entity.EntityState.Modified;
            Context.Configuration.AutoDetectChangesEnabled = true;
        }

        protected virtual void Update(TEntity entityFrom, TEntity entityTo)
        {
            var attachedEntity = _context.Entry(entityTo);
            attachedEntity.CurrentValues.SetValues(entityFrom);
        }

        protected virtual bool Exists<TEntity>(TEntity entity)
        {
            return this.dbSet.Any(e => e.Equals(entity));
        }
    }
}