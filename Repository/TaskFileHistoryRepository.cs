﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class TaskFileHistoryRepository:BaseRepository<TaskFileHistory>,ITaskFileHistoryRepository
    {
        public TaskFileHistoryRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public TaskFileHistoryRepository() : base(new UnitOfWork(DBSource.Task))
        {
        }

        public void SaveToTaskFileHistory(TaskFileHistoryModel model)
        {
            var taskFileHistory = Mapper.Map<TaskFileHistory>(model);
            taskFileHistory.CreatedBy = UserPrincipal.Current.UserId;
            taskFileHistory.CreatedOn = DateTime.UtcNow;
            this.InsertNew(taskFileHistory);
        }

        public int DeleteChildTaskFile(Guid childTaskFileId)
        {
            var entityToUpdate = this.Find(p => p.ChildTaskFileId == childTaskFileId).FirstOrDefault();
            if (entityToUpdate != null)
            {
                this.Delete(entityToUpdate);
                return 1;
            }
            return 0;
        }

        public bool IsFileHistoryExists(Guid taskFileId)
        {
            var result = this.Find(p => p.ChildTaskFileId == taskFileId).FirstOrDefault();
            return result != null;
        }
    }
}
