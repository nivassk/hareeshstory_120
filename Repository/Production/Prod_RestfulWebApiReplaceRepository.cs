﻿using Model.Production;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Repository.Production
{
    public class Prod_RestfulWebApiReplaceRepository : IProd_RestfulWebApiReplaceRepository
    {
        private IDocumentTypeRespository doumentTypeRepostory;
        public Prod_RestfulWebApiReplaceRepository()
            : this(
                new DocumentTypeRepository())
        {
            
        }
        private Prod_RestfulWebApiReplaceRepository(IDocumentTypeRespository _doumentTypeRepostory)
        {
            doumentTypeRepostory = _doumentTypeRepostory;
        }
        public RestfulWebApiResultModel ReplaceDocumentInfo(RestfulWebApiUpdateModel DocumentInfo, string token, HttpPostedFileBase file)
        {
            RestfulWebApiUploadHelper helper = new RestfulWebApiUploadHelper();
            RestfulWebApiResultModel rslt = new RestfulWebApiResultModel();
            //var DocType = new DocumentTypeModel();
            var fileName = Path.GetFileName(file.FileName);

            //DocType = doumentTypeRepostory.GetDocumentTypeId(fileName);
            //if (DocType.DocumentTypeId > 0)
            //{
            //    DocumentInfo.documentType = DocType.DocumentTypeId.ToString();
            //}
            //else
            //{
            //    DocumentInfo.documentType = "1980";
            //}
            string URL = ConfigurationManager.AppSettings["ReplaceDocument"].ToString() + token;
            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    var values = new[]
                    {
                        new KeyValuePair<string, string>("docId", DocumentInfo.docId),
                        new KeyValuePair<string, string>("documentType", DocumentInfo.documentType),
                        
                       
                         
                    };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }
                    content.Add(new StreamContent(file.InputStream), "\"documents\"", string.Format("\"{0}\"", fileName));

                    rslt = helper.UpdateReplacePostRequest(URL, content);
                    rslt.documentType = DocumentInfo.documentType;
                }
            }
            
            return rslt;
        }

      

    }
}
