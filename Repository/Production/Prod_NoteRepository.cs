﻿using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Production
{
  public  class Prod_NoteRepository :BaseRepository<Prod_Note>,IProd_NoteRepository
    {
       public Prod_NoteRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

       public Prod_NoteRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

       public int AddNote(Model.Production.Prod_NoteModel model)
       {
           var note = Mapper.Map<Prod_Note>(model);
           this.InsertNew(note);         
           UnitOfWork.Save();
           return note.NoteId;
       }

       public List<Model.Production.Prod_NoteModel> GetAllTaskNotes(Guid taskInstanceId)
       {   
           var notes= this.Find(m=>m.TaskInstanceId == taskInstanceId);

           return Mapper.Map<List<Model.Production.Prod_NoteModel>>(notes);
       }

       public void DeleteNote(int noteId)
       {
           var note = this.GetByIDFast(noteId);
           if (note != null)
           {
               this.Delete(note);
               UnitOfWork.Save();
           }
       }
    }
}
