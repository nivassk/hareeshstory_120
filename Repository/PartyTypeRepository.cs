﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class PartyTypeRepository : BaseRepository<PartyType>, IPartyTypeRepository
	{
		public PartyTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public PartyTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<PartyType> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public List<PartyTypeModel> GetPartyTypes()
		{
			var partyTypes = this.GetAll().ToList();
			return Mapper.Map<List<PartyTypeModel>>(partyTypes);
		}
		public string GetPartyTypeName(int pId)
		{
			PartyType objPartyType = this.GetByID(pId);
			if (objPartyType != null)
				return objPartyType.Party;
			return null;
		}
	}
}



