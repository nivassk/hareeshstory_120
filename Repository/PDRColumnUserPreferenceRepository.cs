﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Repository;
using Model;

namespace Repository
{
   public class PDRColumnUserPreferenceRepository:BaseRepository<PDRUserPreference>,IPDRColumnUserPreferenceRepository
    {
        public PDRColumnUserPreferenceRepository(UnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IQueryable<PDRUserPreference> DataToJoin
        {
            get { return DbQueryRoot; }
        }
        public int AddNewColumn(PDRUserPreferenceViewModel model)
        {
            var pdrUserPreference = Mapper.Map<PDRUserPreference>(model);
            this.InsertNew(pdrUserPreference);
            UnitOfWork.Save();
            return pdrUserPreference.PDRUserPreferenceID;
        }

        public void UpdatePDRUserPreference(PDRUserPreferenceViewModel model)
        {
            var modelToUpdate = this.Find(m=>m.PDRColumnID==model.PDRColumnID && m.UserID==model.UserID).FirstOrDefault();
            if (modelToUpdate != null)
            {
                modelToUpdate.PDRColumnID = model.PDRColumnID;
                modelToUpdate.Visibility = model.Visibility;
                this.Update(modelToUpdate);
            }
        }

        public IEnumerable<PDRUserPreferenceViewModel> GetUserPreferenceByUser(int userId)
        {
            var pdrUserPreference = this.Find(m => m.UserID == userId).ToList();
            return Mapper.Map<IEnumerable<PDRUserPreferenceViewModel>>(pdrUserPreference);
        }

        public void DeletePDRUserPreference(int userID, int pdrColumnID)
        {
            var entityToDelete = this.Find(m => m.UserID == userID && m.PDRColumnID == pdrColumnID).FirstOrDefault();
            this.Delete(entityToDelete);
            UnitOfWork.Save();
        }

        public void SetDefaultUserPreference(int userID)
        {
            var entities = this.Find(m => m.UserID == userID).ToList();
            if(entities.Count() > 0)
            {
                foreach (var entityToDelete in entities)
                {
                    if (entityToDelete != null)
                    {
                        this.Delete(entityToDelete);
                        UnitOfWork.Save();
                    }
                }
                
            }
        }
    }
}
