﻿using System.Linq;

namespace Repository.Interfaces
{
    public interface IJoinable<TEntity> where TEntity : class
    {
        IQueryable<TEntity> DataToJoin { get; }
    }
}