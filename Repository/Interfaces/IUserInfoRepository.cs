﻿
using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUserInfoRepository
    {
        UserInfoModel GetUserInfoByUsername(string sUserName);
        List<UserInfoModel> GetProductionUsers();
		List<UserInfoModel> GetProductionWLMUsers();

		UserViewModel GetUserInfoById(int userId);
        List<UserInfoModel> GetUnAssignedProductionUsers();

        List<UserInfoModel> GetUnAssignedExternalReviewers();

        List<UserInfoModel> GetALLInternalSpecialOptionUsers();


    }
}
