﻿
using System;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface ITaskConcurrencyRepository
    {
        byte[] GetConcurrencyTimeStamp(Guid taskInstanceId);
        void UpdateTaskInstance(TaskModel taskModel);
        byte[] AddTaskInstance(TaskModel taskModel);
    }
}
