﻿using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IUploadDataSummaryRepository
    {
        System.Collections.Generic.IEnumerable<ReportExcelUpload_Model> GetDataUploadSummaryByUserId(int iUserId);
    }
}
