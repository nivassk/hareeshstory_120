﻿using EntityObject.Entities.HCP_intermediate;

namespace Repository.Interfaces
{
    public interface ICommentViewRepository : IJoinable<vwComment>
    {
    }
}
