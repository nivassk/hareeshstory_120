﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetOpaHistory_FolderList]
(
@TaskInstanceId uniqueidentifier
)
AS

BEGIN

declare @newFileReqCount int

select @newFileReqCount = count([NewFileRequestId]) from [$(DatabaseName)].[dbo].[NewFileRequest] nfr
inner join [dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId
left join [$(DatabaseName)].[dbo].[Prod_TaskXref] xref on xref.[TaskXrefid] = pc.ParentTaskInstanceId
where xref.[TaskInstanceId]= @TaskInstanceId


 select distinct      
					 fs.[FolderKey] as FolderKey,
                     fs.[FolderName] as FolderName,
					 fs.[SubfolderSequence] as [SubfolderSequence],
                     fs.FolderSortingNumber

                     from [$(DatabaseName)].dbo.Prod_FolderStructure fs 
                     left join [$(DatabaseName)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
                     left join [$(DatabaseName)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
					 
				     where (tfile.TaskInstanceId = @TaskInstanceId and fs.ViewTypeId in (1,4)) or fs.[FolderKey] = 2 or   
					(fs.[FolderKey] =1 and @newFileReqCount > 0)

union
select distinct      
					 sfs.[FolderKey] as FolderKey,
                     sfs.[FolderName] as FolderName,
					 sfs.[SubfolderSequence] as [SubfolderSequence],
                     sfs. FolderSortingNumber
                     from [$(DatabaseName)].dbo.Prod_SubFolderStructure sfs 
                     left join [$(DatabaseName)].dbo.TaskFile_FolderMapping tf on sfs.[FolderKey] = tf.[FolderKey]
                     left join [$(DatabaseName)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
					 
					 where tfile.TaskInstanceId = @TaskInstanceId 

end


---- Old------
--CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetOpaHistory_FolderList]
--(
--@TaskInstanceId uniqueidentifier
--)
--AS

--BEGIN

--declare @newFileReqCount int

--select @newFileReqCount = count([NewFileRequestId]) from [$(DatabaseName)].[dbo].[NewFileRequest] nfr
--inner join [dbo].[ParentChildTask] pc on pc.ChildTaskInstanceId = nfr.ChildTaskInstanceId
--left join [$(DatabaseName)].[dbo].[Prod_TaskXref] xref on xref.[TaskXrefid] = pc.ParentTaskInstanceId
--where xref.[TaskInstanceId]= @TaskInstanceId


-- select distinct      
--					 fs.[FolderKey] as FolderKey,
--                     fs.[FolderName] as FolderName,
--					  fs.[SubfolderSequence] as [SubfolderSequence]
                    
--                     from [$(DatabaseName)].dbo.Prod_FolderStructure fs 
--                     left join [$(DatabaseName)].dbo.TaskFile_FolderMapping tf on fs.[FolderKey] = tf.[FolderKey]
--                     left join [$(DatabaseName)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
					 
--					 where (tfile.TaskInstanceId = @TaskInstanceId and fs.ViewTypeId=1) or fs.[FolderKey] = 2 or 
--					 (fs.[FolderKey] =1 and @newFileReqCount > 0)

--union
--select distinct      
--					 sfs.[FolderKey] as FolderKey,
--                     sfs.[FolderName] as FolderName,
--					 sfs.[SubfolderSequence] as [SubfolderSequence]
                    
--                     from [$(DatabaseName)].dbo.Prod_SubFolderStructure sfs 
--                     left join [$(DatabaseName)].dbo.TaskFile_FolderMapping tf on sfs.[FolderKey] = tf.[FolderKey]
--                     left join [$(DatabaseName)].dbo.TaskFile tfile on tf.[TaskFileId] = tfile.TaskFileId
					 
--					 where tfile.TaskInstanceId = @TaskInstanceId 

--end

