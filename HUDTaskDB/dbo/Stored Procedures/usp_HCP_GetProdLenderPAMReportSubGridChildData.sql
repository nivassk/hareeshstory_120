﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetProdLenderPAMReportSubGridChildData]
(
	@taskInstanceId uniqueidentifier
)
AS
/*
Date			Changed by					Reason
====			===========					======	
04/02/2018		Suresh,Rashmi,Venkatesh		Filter the results by lender roles (LAM,BAM,LAR) 

*/ 
BEGIN

	CREATE TABLE #LenderPAMReport
	(
		taskInstanceId uniqueidentifier,
		FHANumber NVARCHAR(15), 
		ProjectName NVARCHAR(100), 
		ProjectTypename NVARCHAR(100),
		ProductionTaskType NVARCHAR(100),
		StartDate DATETIME,
		EndDate DATETIME,
		DaysActive int,
		LoanAmount decimal(19,2),
		LenderName NVARCHAR(100),
		LenderRole varchar(50),
		IsTaskReassigned bit,
		StatusId int,
		ProcessedBy NVARCHAR(100),
		ViewName  varchar(50),
		IsRAI bit

	);
	
	-- Gets Xref Tasks for Application Req Types
	INSERT INTO #LenderPAMReport
	Select t.TaskInstanceId as taskInstanceId,
		   t.[FhaNumber],
		   fr.ProjectName,
		   pt.ProjectTypeName,
		   pg.PageTypeDescription as ProductionTaskType,
		   txref.ModifiedOn as StartDate,
		   (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN tend.StartTime ELSE null END) as  enddate,
		   (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive, 		   
		   fr.LoanAmount,
		   t.AssignedBy as LenderName,
		   r.RoleName as LenderRole,
		   (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
		   ts.TaskStepId as StatusId,
		  -- xrefAut.FirstName + ' ' + xrefAut.LastName as ProcessedBy,
		    xrefAut.UserName  as ProcessedBy,
		   pv.ViewName as ViewName,
		   (CASE WHEN pc.ChildTaskInstanceId is null THEN 0 ELSE 1 END) as IsRAI 

	from [$(DatabaseName)].dbo.Prod_TaskXref txref
	inner join [$(LiveDB)].dbo.HCP_Authentication xrefAut on xrefAut.UserID = txref.AssignedTo
	inner join [$(DatabaseName)].dbo.Prod_View pv on pv.ViewId = txref.ViewId
	inner join [$(DatabaseName)].dbo.Task t on t.TaskInstanceId = txref.TaskInstanceId and t.SequenceId = 0
	inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.FHANumber = t.FHANumber
	inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
	inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
	left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
			  inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
						 from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
						 )tend on tend.TaskInstanceId= t.TaskInstanceId 
	inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
	inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
	inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
	inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
	left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
	
	where txref.TaskInstanceId = @taskInstanceId and txref.ViewId not in (8,9,10)
	   and   r.RoleName in ('LenderAccountManager','BackupAccountManager','LenderAccountRepresentative') 
	-- Gets Child Tasks for Application Req Types in Task Table
	 INSERT INTO #LenderPAMReport	 
	Select t.TaskInstanceId as taskInstanceId,
		   t.[FhaNumber],
		   fr.ProjectName,
		   pt.ProjectTypeName,
		   pg.PageTypeDescription as ProductionTaskType,
		   t.StartTime as StartDate,
		   (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN tend.StartTime ELSE null END) as  enddate,
		   (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 15) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive, 		   
		   fr.LoanAmount,
		   t.AssignedTo as LenderName,
		   r.RoleName as LenderRole,
		   (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
		   ts.TaskStepId as StatusId,
		   t.AssignedBy as ProcessedBy,
		   'RAI' as ViewName,
		   (CASE WHEN pc.ChildTaskInstanceId is null THEN 0 ELSE 1 END) as IsRAI 


	from [$(DatabaseName)].dbo.Task t
	inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.FHANumber = t.FHANumber
	inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
	inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
	left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
			  inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
						 from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
						 )tend on tend.TaskInstanceId= t.TaskInstanceId 
	inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
	inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
	
	inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
	inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
	left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
	
	where t.SequenceId = 0   and
		t.TaskInstanceId in( (select pc.ChildTaskInstanceId from [$(DatabaseName)].dbo.ParentChildTask pc 
													 where pc.ParentTaskInstanceId in (select xref.TaskXrefid from [$(DatabaseName)].dbo.Prod_TaskXref xref where xref.TaskInstanceId = @taskInstanceId)))
       and   r.RoleName in ('LenderAccountManager','BackupAccountManager','LenderAccountRepresentative') 
	-- Gets Xref Tasks for Insert FHA
	INSERT INTO #LenderPAMReport
	Select txref.TaskInstanceId as taskInstanceId,
		   (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN tend.FhaNumber ELSE null END) as FhaNumber,
		   fr.ProjectName,
		   pt.ProjectTypeName,
		   pg.PageTypeDescription as ProductionTaskType,
		   txref.ModifiedOn as StartDate,
		   (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN tend.StartTime ELSE null END) as  enddate,
		   (CASE WHEN (tend.TaskStepId is not null and tend.TaskStepId= 19) THEN DATEDIFF(DAY,t.StartTime,tend.StartTime) ELSE DATEDIFF(DAY,t.StartTime,GETUTCDATE()) END) as DaysActive, 		   
		   fr.LoanAmount,
		   t.AssignedBy as LenderName,
		   r.RoleName as LenderRole,
		   (CASE WHEN t.IsReassigned is null THEN 0 ELSE t.IsReassigned END) as IsTaskReassigned , 
		   ts.TaskStepId as StatusId,
		    --xrefAut.FirstName + ' ' + xrefAut.LastName as ProcessedBy,
			 xrefAut.UserName as ProcessedBy,
		   pv.ViewName as ViewName,
		   (CASE WHEN pc.ChildTaskInstanceId is null THEN 0 ELSE 1 END) as IsRAI 

	from [$(DatabaseName)].dbo.Prod_TaskXref txref
	inner join [$(LiveDB)].dbo.HCP_Authentication xrefAut on xrefAut.UserID = txref.AssignedTo
	inner join [$(DatabaseName)].dbo.Prod_View pv on pv.ViewId = txref.ViewId
	inner join [$(DatabaseName)].dbo.Task t on t.TaskInstanceId = txref.TaskInstanceId and SequenceId = 0
	inner join [$(LiveDB)].dbo.[Prod_FHANumberRequest] fr on fr.TaskinstanceId = t.TaskInstanceId
	inner join [$(LiveDB)].[dbo].[Prod_ProjectType] pt on pt.ProjectTypeId = fr.ProjectTypeId
	inner join  [$(LiveDB)].[dbo].HCP_PageType pg on pg.PageTypeId = t.PageTypeId
	left join(select tfinal.* from [$(DatabaseName)].dbo.Task tfinal
			  inner join (select t1.TaskInstanceId,max(starttime) as taskstarttime
						 from  [$(DatabaseName)].dbo.Task t1 group by t1.TaskInstanceId ) tmaxdate on tmaxdate.TaskInstanceId = tfinal.TaskInstanceId and tmaxdate.taskstarttime = tfinal.StartTime
						 )tend on tend.TaskInstanceId= t.TaskInstanceId 
	inner join [$(LiveDB)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
	inner join [$(LiveDB)].dbo.webpages_UsersInRoles ur on ur.UserId = a.UserID
	inner join [$(LiveDB)].dbo.webpages_Roles r on r.RoleId = ur.RoleId
	inner join [$(DatabaseName)].dbo.TaskStep ts on ts.TaskStepId = tend.TaskStepId
	left join [$(DatabaseName)].dbo.ParentChildTask pc on pc.ChildTaskInstanceId = t.TaskInstanceId
	
	where txref.TaskInstanceId =  @taskInstanceId and txref.ViewId in (8,9,10)
	   and   r.RoleName in ('LenderAccountManager','BackupAccountManager','LenderAccountRepresentative') 
	SELECT DISTINCT taskInstanceId ,
		FHANumber , 
		ProjectName , 
		ProjectTypename ,
		ProductionTaskType ,
		StartDate ,
		EndDate ,
		DaysActive ,
		LoanAmount ,
		LenderName ,
		LenderRole ,
		IsTaskReassigned ,
		StatusId ,
		ProcessedBy ,
		ViewName ,
		IsRAI 

		into #ProjectCompletedDateNull
	FROM #LenderPAMReport 
	where  EndDate is null
	ORDER BY StartDate asc
	
	SELECT DISTINCT taskInstanceId ,
		FHANumber , 
		ProjectName , 
		ProjectTypename ,
		ProductionTaskType ,
		StartDate ,
		EndDate ,
		DaysActive ,
		LoanAmount ,
		LenderName ,
		LenderRole ,
		IsTaskReassigned ,
		StatusId ,
		ProcessedBy ,
		ViewName  ,
		IsRAI
  
		into #ProjectCompletedDateNotNull
	FROM #LenderPAMReport 
	where EndDate is not null
	ORDER BY EndDate desc;

	WITH cte AS 
		(
		select * from #ProjectCompletedDateNull
		),

	cte2 AS 
	(
	select * from #ProjectCompletedDateNotNull
	)

	SELECT ROW_NUMBER() OVER (ORDER BY cte.StartDate) AS ID, * FROM cte
	UNION ALL
	SELECT ROW_NUMBER() OVER (ORDER BY cte2.EndDate desc) AS ID,* FROM cte2 

END

