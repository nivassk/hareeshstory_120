﻿CREATE  PROCEDURE [dbo].[USP_GetProductionTasksByUserName] (
@userName nvarchar(50),
@Role nvarchar(50)
)
AS
/*
Change History
==============
Date			Changed by				Reason
20180127		skumar					Project name on the grid under Applciation Process is not in sync with Lender's original data entry (Bug #732
20180224		skumar					Form290 status is updated under form290 union 
20180517		skumar					List AmendmentRequest
06/22/2018		rganapathy				Task#3297-Form290 displaying all other users tasks in My Production task - Fixed + Assigned To should be emailaddress
06/27/2018		rganapathy				Task#3297-Corrected duration for Form 290 Completed records and query to get only latest sequenceid task for form290
07/17/2018		skumar					Bug#3661- Assigned to - Name v/s Email in My Production Task 
09/30/2018		skumar					Assign WLM task from Closer for amendment template signature
11/01/2018		skumar					US#1851 .A task will be created for the Closer to complete the DAP. DAP is directly assigned to UW/Closer of this Amendment task
12/04/2018		skumar					US#4805 .Verify the stored procedure which filters the tasks based on user
*/
BEGIN
declare @userId int

set @userId= (select UserID from [$(LiveDB)].dbo.HCP_Authentication where UserName= @userName)
      
if (@Role = 'ProductionUser' or @Role= 'ProductionWlm' or @Role ='HUDAdmin' or @Role= 'Reviewer')
(       
	--FHA# REQUEST Insert
     SELECT fhaRequest. ProjectName +'(' + pageType. PageTypeDescription +')' as TaskName
			, fhaRequest .ProjectName 
			, task. TaskInstanceId as ParentChildInstanceId 
			, lender . Lender_Name as LenderName
			, fhaRequest. FHANumberRequestId as FhaRequestInstanceId 
			, task. PageTypeId as FhaRequestType
			--, users . FirstName+ ' ' +users . LastName as assignedTo 
			,users. UserName as assignedTo
			,null as AssignedBy
			, null as groupid 
			, pageType . PageTypeDescription as productiontype 
			, fhaRequest . ModifiedOn as LastUpdated 
			, fhaRequest . Comments as Comments 
			, case	when task.TaskStepId = 17 or task.TaskStepId = 18 then DATEDIFF ( day,fhaRequest . RequestSubmitDate, GETDATE ()) 
					when task.TaskStepId = 19 then DATEDIFF ( day, fhaRequest . RequestSubmitDate, task.StartTime) 
					end AS Duration 
			,taskStep . TaskStepNm as Status
			, taskStep .TaskStepId as StatusId
			, 'FHA# REQUEST' as ProductionTaskType
			, pageType.orderby
			, fhaRequest . CreatedBy
      FROM [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
	  join [$(LiveDB)].dbo.LenderInfo lender on lender. LenderID =fhaRequest. LenderId
      join [$(DatabaseName)].dbo.Task task on fhaRequest. TaskinstanceId= task .TaskInstanceId
      inner join (SELECT MAX( SequenceId ) AS maxSeqId,TaskInstanceId
                   FROM [$(DatabaseName)]. [dbo]. [Task]
                   GROUP BY TaskInstanceId
                  ) TMAX ON task .TaskInstanceId = TMAX. TaskInstanceId AND task .SequenceId = TMAX. maxSeqId
      join [$(LiveDB)].dbo.HCP_Authentication users on users. UserName= task .AssignedTo
      join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType. PageTypeId= task .PageTypeId
      join [$(DatabaseName)].dbo.TaskStep taskStep on task. TaskStepId = taskStep. TaskStepId
      WHERE (task . TaskStepId= 17 or  task . TaskStepId= 18 or task . TaskStepId= 19 )
			and ( task .AssignedTo = @userName or task .AssignedBy = @username)

      UNION

      --FHA# REQUEST Credit Review and Portifolio
      SELECT   fhaRequest. ProjectName +' (' + productiontype. ViewName +')' as TaskName
				, fhaRequest. ProjectName 
				,txref . TaskXrefid as ParentChildInstanceId
				,lender . Lender_Name as lenderName 
				,fhaRequest . FHANumberRequestId asFhaRequestInstanceId
				, txref .ViewId as FhaRequestType
				--, users . FirstName+ ' ' +users . LastName as AssignedTo 
				,users. UserName as AssignedTo
				,null as AssignedBy
				,null as groupid 
				,productiontype . ViewName as productiontype 
				,fhaRequest . ModifiedOn as LastUpdated 
				,fhaRequest . Comments as Comments ,
				--DATEDIFF ( day, fhaRequest . RequestSubmitDate, GETDATE ()) AS Duration, 
				case when txref .status  = 17 or txref .status  = 18 then DATEDIFF ( day,fhaRequest . RequestSubmitDate, GETDATE ()) 
				     when txref .status = 19 then DATEDIFF ( day, fhaRequest . RequestSubmitDate, txref.CompletedOn) 
					end AS Duration 
				, taskStep. TaskStepNm as Status 
				,taskStep . TaskStepId as StatusId 
				,'FHA# REQUEST' as ProductionTaskType 
				, 3 as orderby
				, fhaRequest . CreatedBy
         FROM [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
         join [$(LiveDB)].dbo.LenderInfo lender on lender. LenderID = fhaRequest. LenderId
         join [$(DatabaseName)].dbo.Prod_TaskXref txref on fhaRequest. TaskinstanceId= txref .TaskInstanceId
         join [$(LiveDB)].dbo.HCP_Authentication users on users. UserID= txref .AssignedTo
		 join [$(DatabaseName)].dbo.Prod_View productiontype on productiontype. ViewId= txref .ViewId
         join [$(DatabaseName)].dbo.TaskStep taskStep on txref. status = taskStep. TaskStepId
         WHERE (txref . Status= 18 or txref . Status= 19 ) and txref. AssignedTo =@userId
		  -- Form 290
		 Union
		 (
		   
select '(' + opa. fhaNumber +')' + PA.ProjectTypeName as TaskName, fhaRequest. ProjectName ,task.TaskInstanceId as ParentChildInstanceId ,
                  lender . Lender_Name as lenderName ,fhaRequest . FHANumberRequestId as FhaRequestInstanceId, task.PageTypeId as FhaRequestType,
                  task.AssignedTo as AssignedTo ,task.AssignedBy as AssignedBy,
                        null as groupid ,
                        'Form 290' as productiontype ,
                        task .StartTime as LastUpdated ,
                  opa . aeComments as Comments ,
				    case when (task.TaskstepId = 22) then DATEDIFF ( day,task . StartTime, GETDATE ()) 
				       when (task.TaskstepId = 23) then DATEDIFF ( day,(select top 1 task.Starttime from Task where Taskinstanceid = task.Taskinstanceid and Taskstepid = 22), task . StartTime) end AS Duration,  
						taskStep . TaskStepNm as Status, taskStep .TaskStepId as StatusId,
						pageType. PageTypeDescription as ProductionTaskType ,  pageType.orderby,
                       users.UserID as   CreatedBy

		from dbo.task task 
		inner join ( select [TaskInstanceId] , max ( [SequenceId]) as SequenceId
					  from dbo.[Task]  where Pagetypeid =16
					   group by [TaskInstanceId]) latesttask
		on task.Taskinstanceid = latesttask.Taskinstanceid and task.Sequenceid = latesttask.Sequenceid
		join [$(DatabaseName)].dbo.Prod_Form290Task form290
		                      on form290.TaskInstanceID = task.TaskInstanceId
			join [$(LiveDB)].dbo.opaform opa on opa.TaskInstanceId = form290.ClosingTaskInstanceID
			join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
             on opa. fhaNumber =fhaRequest . fhanumber
			 left join [$(LiveDB)].dbo.[Prod_ProjectType]  PA
		  on PA. ProjecttypeId =opa . Projectactiontypeid
             left join [$(LiveDB)].dbo.LenderInfo lender on lender .LenderID =fhaRequest. LenderId
			 left join [$(LiveDB)].dbo.HCP_Authentication users on users. UserName= task .AssignedTo
             left join [$(LiveDB)].dbo. HCP_PageType as pageType on pageType. PageTypeId= task .PageTypeId
			  join [$(DatabaseName)].dbo.TaskStep taskStep on task. TaskStepId = taskStep. TaskStepId
			where task.PageTypeId = 16 and task.AssignedTo =  @userName
					and (task . TaskStepId= 22 or  task . TaskStepId= 23)
		 )
		 
         Union
		SELECT * FROM 
		(    select CASE  WHEN  txref.ViewId = 14 THEN 'DAP ' + '(' +opa.fhaNumber +')'+ PA.ProjectTypename   
						  ELSE '(' + opa. fhaNumber +')' + PA. ProjectTypename 
						  END   as TaskName
					, fhaRequest. ProjectName 
					,txref . TaskXrefid as ParentChildInstanceId 
					,lender . Lender_Name as lenderName 
					,fhaRequest . FHANumberRequestId as FhaRequestInstanceId
					, txref .ViewId as FhaRequestType
					, FP . AssignedTo as AssignedTo 
					,null as AssignedBy
					,txref . taskinstanceid as groupid 
					,CASE WHEN pageType. PageTypeDescription='AMENDMENTS'  THEN 'Amendment Request'
						 else productiontype . ViewName 
						 end as productiontype 
                     -- productiontype . ViewName as productiontype , 
					,FP . starttime as LastUpdated 
					,opa . aeComments as Comments 
					,case when txref .status = 17 or txref .status = 18 then DATEDIFF ( day, txref.ModifiedOn, GETDATE ()) 
						  when txref .status = 15 then DATEDIFF ( day, txref.ModifiedOn, txref.CompletedOn) 
						end AS Duration 
					, taskStep. TaskStepNm as  Status 
					,txref .status as StatusId
					,CASE WHEN pageType. PageTypeDescription='AMENDMENTS'  THEN 'AMENDMENTS REQUEST'
						 else pageType. PageTypeDescription
						 end as ProductionTaskType 
					--, pageType. PageTypeDescription as ProductionTaskType 
					,  pageType.orderby,
                       txref . assignedto as   CreatedBy
			 FROM [$(LiveDB)].dbo.opaform opa
			 join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest on opa. fhaNumber =fhaRequest . fhanumber
             left join [$(LiveDB)].dbo. LenderInfo lender on lender .LenderID =fhaRequest. LenderId
             join [$(DatabaseName)].dbo.Prod_TaskXref txref on opa. TaskinstanceId= txref .TaskInstanceId
             left join   (select ct .* from [$(DatabaseName)].dbo.[Task] ct
             inner join
						( select [TaskInstanceId] , max ( [SequenceId]) as SequenceId
                        from [$(DatabaseName)].dbo.[Task] mct  
						group by [TaskInstanceId] 
						) mct on mct .[TaskInstanceId] = ct. [TaskInstanceId] and mct . SequenceId = ct. SequenceId ) fp 
				on FP . TaskinstanceId= txref .TaskInstanceId
             left join [$(LiveDB)].[dbo] . [Prod_ProjectType]  PA on PA. ProjecttypeId =opa . Projectactiontypeid
             left join [$(LiveDB)].dbo. HCP_Authentication users on users. UserID= txref .AssignedTo
             left join [$(LiveDB)].dbo. HCP_PageType as pageType on pageType. PageTypeId= FP .PageTypeId
             left join [$(DatabaseName)].dbo. Prod_View productiontype on productiontype. ViewId= txref .ViewId
             left join [$(DatabaseName)].dbo. TaskStep taskStep on txref .status = taskStep. TaskStepId and txref .AssignedTo = @userId
			 --where  ( FP .AssignedTo = @userName or FP .AssignedBy = @username)
			 where  ( FP .AssignedTo = @userName or FP .AssignedBy = @username or (txref .ViewId=13 or txref .ViewId=14))
			 
			union

			select CASE WHEN fp.TaskStepId =  20 THEN 'FC Request ' +'(' +opa.fhaNumber +')'+ PA.ProjectTypename
						WHEN fp.TaskStepId =  21 THEN 'FC Response ' +'(' +opa.fhaNumber +')'+ PA.ProjectTypename
						-- WHEN  xref.ViewId = 14 THEN 'DAP ' + '(' +opa.fhaNumber +')'+ PA.ProjectTypename 
						ELSE 'RAI ' +'(' +opa.fhaNumber +')'+ PA.ProjectTypename
						END	  as TaskName
					,fhaRequest.ProjectName,pct.childtaskinstanceid as parenttaskinstanceid
					,lender.Lender_Name as lenderName
					,opa.projectactionformid as FhaRequestInstanceId
					,xref.ViewId as FhaRequestType
					,  fp.assignedto as AssignedTo 
					,null as AssignedBy, xref.taskinstanceid as groupid
					,  productiontype.ViewName as productiontype
					,fp.starttime as LastUpdated,null as Comments
					,case	when fp.TaskStepId = 20 then DATEDIFF ( day,fp.starttime, GETDATE ()) 
							when fp.TaskStepId = 21 then DATEDIFF ( day, (select top 1 starttime from Task where taskstepid = 20 and TaskInstanceId = fp.taskinstanceid),fp.starttime ) 
							when  fp.TaskStepId = 16 then DATEDIFF ( day,fp.starttime, GETDATE ()) 
							when  fp.TaskStepId = 15 then DATEDIFF ( day,(select top 1 starttime from Task where taskstepid in (16,20) and TaskInstanceId = fp.taskinstanceid), fp.starttime) 
							end AS Duration 
					,'RAI' as status
					,fp.TaskStepId as StatusId
					,pageType.pagetypedescription as ProductionTaskType
					,  pageType.orderby, xref.assignedto as  CreatedBy 
			from parentchildtask pct 
			join Prod_TaskXref  xref on pct.parenttaskinstanceid=xref.taskxrefid
			join( select * from task t
							where   TaskId in (select max ( unqueTask. TaskId ) from task unqueTask
												group by unqueTask .TaskInstanceId )
				 ) as fp on fp.taskinstanceid=pct.childtaskinstanceid
			join  [$(LiveDB)].dbo.opaform opa on opa.taskinstanceid=xref.taskinstanceid
			join [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest on opa.fhaNumber=fhaRequest.fhanumber
			left join [$(LiveDB)].dbo.LenderInfo lender on lender.LenderID = fhaRequest.LenderId
            left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA		on PA. ProjecttypeId=opa.Projectactiontypeid
			left join [$(LiveDB)].dbo.HCP_Authentication users on users.UserID=xref.AssignedTo
            left join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType.PageTypeId=FP.PageTypeId
            left join [$(DatabaseName)].dbo.Prod_View productiontype on productiontype.ViewId=xref.ViewId 
            left join [$(DatabaseName)].dbo.TaskStep taskSteps on xref.status = taskSteps.TaskStepId 
			 --WHERE xref.Status=18
                
       ) as t  
	   where t. CreatedBy =@userId or (t.FhaRequestType=13 or t.FhaRequestType=14) 
)
ELSE IF (@Role = 'LenderAccountRepresentative' OR @Role = 'LenderAccountManager' OR   @Role = 'BackupAccountManager' )
(
	SELECT   fhaRequest. ProjectName +'(' + pageType. PageTypeDescription +')' as TaskName, fhaRequest. ProjectName ,task . TaskInstanceId as ParentChildInstanceId ,
                      lender . Lender_Name as LenderName, fhaRequest. FHANumberRequestId as FhaRequestInstanceId , task. PageTypeId as FhaRequestType,
                      CASE WHEN users .UserID is null then 'Not Assigned' else   users. FirstName+ ' ' +users . LastName  end as AssignedTo ,null as AssignedBy ,null as groupid,
                      pageType . PageTypeDescription as productiontype ,
                      fhaRequest . ModifiedOn as LastUpdated ,
                      fhaRequest . Comments as Comments , 
					  --DATEDIFF (day , fhaRequest . RequestSubmitDate, GETDATE ()) AS Duration ,
					  case when task.TaskStepId = 17 or task.TaskStepId = 18 then DATEDIFF ( day,fhaRequest . RequestSubmitDate, GETDATE ()) 
					  when task.TaskStepId = 19 then DATEDIFF ( day, fhaRequest . RequestSubmitDate, task.StartTime) end AS Duration ,
                        taskStep . TaskStepNm as Status, taskStep .TaskStepId as StatusId, pageType. PageTypeDescription as ProductionTaskType ,pagetype.orderby,
                        fhaRequest . CreatedBy, CASE WHEN fhaRequest. IsReadyForApplication = 0 THEN 'No' else 'Yes' end as IsReadyForApplication
              FROM [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
                   join [$(LiveDB)].dbo.LenderInfo lender on lender. LenderID =fhaRequest. LenderId
                 join( select * from task t
                                   where   TaskId in (select max ( unqueTask. TaskId ) from task unqueTask
                                          group by unqueTask .TaskInstanceId )
                                          ) as task on fhaRequest. TaskinstanceId= task .TaskInstanceId
                  left join [$(LiveDB)].dbo. HCP_Authentication users on users. UserName= task .AssignedTo
                  join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType. PageTypeId= task .PageTypeId
                     join [$(DatabaseName)].dbo.TaskStep taskStep on task. TaskStepId = taskStep. TaskStepId
          WHERE   fhaRequest. CreatedBy =@userId

          Union

      
		  select  CASE WHEN xx .TaskStepId =   20 THEN 'FC Request ' + '(' + opa. fhaNumber + ')'+ PA .ProjectTypename
		   WHEN xx . TaskStepId =   21 THEN 'FC Response ' + '(' + opa .fhaNumber + ')'+ PA. ProjectTypename
			WHEN xx . ischild= 1  THEN 'RAI ' + '(' + opa .fhaNumber + ')'+ PA .ProjectTypename
		  ELSE '(' + opa .fhaNumber + ')'+ PA .ProjectTypename
		  End as TaskName, 
		  --PA .ProjectTypename as ProjectName, 
		  opa.PropertyName as ProjectName,xx .taskinstanceid asParentChildInstanceId,
		   lender .Lender_Name as LenderName, opa .projectactionformid  as FhaRequestInstanceId,xx. PageTypeId as FhaRequestType , xx. AssignedTo as AssignedTo, xx. AssignedBy as AssignedBy ,
		   XX .parenttaskinstanceid as groupid,
		   pageType .PageTypeDescription as productiontype,
		   xx .starttime as LastUpdated,
		   xx .notes as Comments,
			--DATEDIFF( day ,xx . starttime, GETDATE ()) as Duration,
			case when xx.TaskStepId = 20 then DATEDIFF ( day,xx.starttime, GETDATE ()) 
			when xx.TaskStepId = 21 then DATEDIFF ( day, (select top 1 starttime from Task where taskstepid = 20 and TaskInstanceId = xx.taskinstanceid),xx.starttime ) 
			when xx.Ischild = 1 and xx.TaskStepId = 16 then DATEDIFF ( day,xx.starttime, GETDATE ()) 
			when xx.Ischild = 1 and xx.TaskStepId = 15 then DATEDIFF ( day,(select top 1 starttime from Task where taskstepid in (16,20) and TaskInstanceId = xx.taskinstanceid), xx.starttime) 
			when xx.Ischild is null and (xx.TaskStepId = 14 or xx.TaskStepId = 17 or xx.TaskStepId = 18) then DATEDIFF ( day, xx.starttime, GETDATE ()) 
			when xx.Ischild is null and xx.TaskStepId = 15 then DATEDIFF ( day, (select top 1 starttime from Task where sequenceid = 0 and TaskInstanceId = xx.taskinstanceid), xx.starttime)
			  end AS Duration ,
			   taskSteps . TaskStepNm as status,
			   xx . TaskStepId as StatusId ,
			   pageType . pagetypedescription as ProductionTaskType ,pagetype.orderby,opa . CreatedBy,
				'Yes'   as IsReadyForApplication from
				( SELECT Ct. TaskId, Ct .Taskinstanceid , Ct. SequenceId ,Ct . AssignedBy, Ct .AssignedTo , Ct. StartTime ,Ct . Notes, Ct .TaskStepId , Ct. PageTypeId ,Ct . FhaNumber, CASE WHEN XX .Ischild is null then Ct. Taskinstanceid else XX . parenttaskinstanceid  end as parenttaskinstanceid, xx. ischild  FROM
					(   select * from (select ct .* from [$(DatabaseName)].dbo.[Task] ct
										 inner join
										 ( select [TaskInstanceId] , max ( [SequenceId]) as SequenceId
										  from [$(DatabaseName)].dbo.[Task] mct  group by [TaskInstanceId] ) mct on mct .[TaskInstanceId] = ct. [TaskInstanceId] and mct . SequenceId = ct. SequenceId ) ct ) ct
					LEFT JOIN (select * from ( select pct . childtaskinstanceid aS taskinstanceid , xref. taskinstanceid as parenttaskinstanceid , fp. assignedby , fp . assignedto , 1 as Ischild, taskSteps. TaskStepId ,'RAI' as status ,fp . starttime from parentchildtask pct
						join Prod_TaskXref xref on pct .parenttaskinstanceid = xref. taskxrefid
						LEFT JOIN Task tk on xref . TaskInstanceId= tk .taskinstanceid

						LEFT JOIN (SELECT * from Task  )
							 as fp on fp .TaskInstanceId = pct. childtaskinstanceid
						join [$(DatabaseName)].dbo.TaskStep taskSteps on fp. TaskStepId = taskSteps . TaskStepId

					   union

					   select tk. taskinstanceid ,tk . taskinstanceid as parenttaskinstanceid ,  tk. assignedby, tk .assignedto , null as Ischild , 
					   taskSteps. TaskStepId ,taskSteps . TaskStepnm as status, tk .starttime from task tk
						join Prod_TaskXref xref on xref .taskinstanceid = tk. taskinstanceid
						join [$(DatabaseName)].dbo.TaskStep taskSteps on tk. TaskStepId = taskSteps . TaskStepId where   tk. TaskStepId!= 17 ) as XX
					) xx
				ON ct. taskinstanceid = xx . taskinstanceid
			) as XX
		  join [$(LiveDB)].dbo.opaform opa

		  on opa . taskinstanceid= XX .parenttaskinstanceid

		  left join [$(LiveDB)].dbo.[Prod_ProjectType]  PA

		on PA. ProjecttypeId =opa . Projectactiontypeid
		left join [$(LiveDB)].dbo. Prod_FHANumberRequest fhaRequest
		on opa. fhaNumber =fhaRequest . fhanumber
		left    join [$(LiveDB)].dbo. LenderInfo lender on lender .LenderID = fhaRequest. LenderId
		join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType .PageTypeId = XX. PageTypeId
		join [$(DatabaseName)].dbo.TaskStep taskSteps on XX. TaskStepId = taskSteps . TaskStepId
		where xx. AssignedTo =@userName or xx. AssignedBy =@userName

  )

  ELSE IF (@Role = 'InspectionContractor' )
  (
     SELECT   fhaRequest. ProjectName +'(' + pageType. PageTypeDescription +')' as TaskName, fhaRequest. ProjectName ,task . TaskInstanceId as ParentChildInstanceId ,
                      '' as LenderName ,fhaRequest . FHANumberRequestId asFhaRequestInstanceId, task. PageTypeId as FhaRequestType ,
                      CASE WHEN users .UserID is null then 'Not Assigned' else   users. FirstName+ ' ' +users . LastName  end as AssignedTo ,null as AssignedBy ,null as groupid,
                      pageType . PageTypeDescription as productiontype ,
                      fhaRequest . ModifiedOn as LastUpdated ,
                      fhaRequest . Comments as Comments , 
					  --DATEDIFF (day ,fhaRequest . RequestSubmitDate, GETDATE ()) AS Duration ,
					  case when task.TaskStepId = 17 or task.TaskStepId = 18 then DATEDIFF ( day,fhaRequest . RequestSubmitDate, GETDATE ()) 
					  when task.TaskStepId = 19 then DATEDIFF ( day, fhaRequest . RequestSubmitDate, task.StartTime) end AS Duration ,
					  taskStep . TaskStepNm as Status, taskStep .TaskStepId as StatusId, pageType. PageTypeDescription as ProductionTaskType ,pagetype.orderby,
					  fhaRequest . CreatedBy, CASE WHEN fhaRequest. IsReadyForApplication = 0 THEN 'No' else 'Yes' end as IsReadyForApplication
              FROM [$(LiveDB)].dbo.Prod_FHANumberRequest fhaRequest
                 
                 join( select * from task t
                                   where   TaskId in (select max ( unqueTask. TaskId ) from task unqueTask
                                          group by unqueTask .TaskInstanceId )
                                          ) as task on fhaRequest. TaskinstanceId= task .TaskInstanceId
                  left join [$(LiveDB)].dbo. HCP_Authentication users on users. UserName= task .AssignedTo
                  join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType. PageTypeId= task .PageTypeId
                     join [$(DatabaseName)].dbo.TaskStep taskStep on task. TaskStepId = taskStep. TaskStepId
          WHERE   fhaRequest. CreatedBy =@userId

          UNION

      
		  select  CASE WHEN xx .TaskStepId =   20 THEN 'FC Request ' + '(' + opa. fhaNumber + ')'+ PA .ProjectTypename
		   WHEN xx . TaskStepId =   21 THEN 'FC Response ' + '(' + opa .fhaNumber + ')'+ PA. ProjectTypename
			WHEN xx . ischild= 1  THEN 'RAI ' + '(' + opa .fhaNumber + ')'+ PA .ProjectTypename
		  ELSE '(' + opa .fhaNumber + ')'+ PA .ProjectTypename
		  End as TaskName, PA .ProjectTypename as ProjectName, xx .taskinstanceid asParentChildInstanceId,
		   lender .Lender_Name as LenderName, opa .projectactionformid  as FhaRequestInstanceId,xx. PageTypeId as FhaRequestType , 
		   xx. AssignedTo as AssignedTo, xx. AssignedBy as AssignedBy ,
		   XX .parenttaskinstanceid as groupid,
		   pageType .PageTypeDescription as productiontype,
		   xx .starttime as LastUpdated,
		   xx .notes as Comments,
		   --DATEDIFF( day ,xx . starttime, GETDATE ()) as Duration,
		   case when xx.TaskStepId = 20 then DATEDIFF ( day,xx.starttime, GETDATE ()) 
			when xx.TaskStepId = 21 then DATEDIFF ( day, (select top 1 starttime from Task where taskstepid = 20 and TaskInstanceId = xx.taskinstanceid),xx.starttime ) 
			when xx.Ischild = 1 and xx.TaskStepId = 16 then DATEDIFF ( day,xx.starttime, GETDATE ()) 
			when xx.Ischild = 1 and xx.TaskStepId = 15 then DATEDIFF ( day,(select top 1 starttime from Task where taskstepid = 16 and TaskInstanceId = xx.taskinstanceid), xx.starttime) end AS Duration ,
			taskSteps . TaskStepNm as status,
			   xx . TaskStepId as StatusId ,
			   pageType . pagetypedescription as ProductionTaskType ,pagetype.orderby,opa . CreatedBy,
				'Yes'   as IsReadyForApplication from
				( SELECT Ct. TaskId, Ct .Taskinstanceid , Ct. SequenceId ,Ct . AssignedBy, Ct .AssignedTo , Ct. StartTime ,Ct . Notes, Ct .TaskStepId , Ct. PageTypeId ,Ct . FhaNumber, CASE WHEN XX .Ischild is null then Ct. Taskinstanceid else XX . parenttaskinstanceid  end as parenttaskinstanceid, xx. ischild  FROM
				(select * from (select ct .* from [$(DatabaseName)].dbo.[Task] ct
					inner join
					( select [TaskInstanceId] , max ( [SequenceId]) as SequenceId
					from [$(DatabaseName)].dbo.[Task] mct  group by [TaskInstanceId] ) mct 
					on mct .[TaskInstanceId] = ct. [TaskInstanceId] and mct . SequenceId = ct. SequenceId ) ct ) ct
					LEFT JOIN (select * from 
							( select pct . childtaskinstanceid aS taskinstanceid , xref. taskinstanceid as parenttaskinstanceid , 
							fp. assignedby , fp . assignedto , 1 as Ischild, taskSteps. TaskStepId ,'RAI' as status ,fp . starttime from parentchildtask pct
							join Prod_TaskXref xref on pct .parenttaskinstanceid = xref. taskxrefid
							LEFT JOIN Task tk on xref . TaskInstanceId= tk .taskinstanceid

							LEFT JOIN (SELECT * from Task  )
									as fp on fp .TaskInstanceId = pct. childtaskinstanceid
							join [$(DatabaseName)].dbo.TaskStep taskSteps on fp. TaskStepId = taskSteps . TaskStepId
       
							union

							select tk. taskinstanceid ,tk . taskinstanceid as parenttaskinstanceid ,  tk. assignedby, tk .assignedto , null as Ischild , taskSteps. TaskStepId ,taskSteps . TaskStepnm as status, tk .starttime from task tk
							join Prod_TaskXref xref on xref .taskinstanceid = tk. taskinstanceid
							join [$(DatabaseName)].dbo.TaskStep taskSteps on tk. TaskStepId = taskSteps . TaskStepId where   tk. TaskStepId!= 17 
							) as XX
					) xx
				ON ct. taskinstanceid = xx . taskinstanceid
				) as XX
		join [$(LiveDB)].dbo.opaform opa

		on opa . taskinstanceid= XX .parenttaskinstanceid

		left join [$(LiveDB)].dbo.[Prod_ProjectType]  PA

		on PA. ProjecttypeId =opa . Projectactiontypeid
		left join [$(LiveDB)].dbo. Prod_FHANumberRequest fhaRequest
		on opa. fhaNumber =fhaRequest . fhanumber
		left    join [$(LiveDB)].dbo.LenderInfo lender on lender .LenderID = fhaRequest. LenderId
		join [$(LiveDB)].dbo.HCP_PageType as pageType on pageType .PageTypeId = XX. PageTypeId
		join [$(DatabaseName)].dbo.TaskStep taskSteps on XX. TaskStepId = taskSteps . TaskStepId
		where xx. AssignedTo =@userName or xx. AssignedBy =@userName

  )

END
