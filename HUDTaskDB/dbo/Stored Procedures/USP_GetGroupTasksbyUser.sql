﻿CREATE  PROCEDURE [dbo].[USP_GetGroupTasksbyUser](

@UserId int,
@LenderId int
)
AS
/*
Date		Changed by					Reason
====		===========					========
04/05/2018	Rashmi,Venkatesh,Suresh		Comment result by userid
10/05/2018  Rashmi,Venkatesh			Bug 4469: FHA# Request Save records in group task check for LenderID
11/01/2018	Rashmi						Bug 4638: To populate Save-Checkin Records in My Production Group Tasks 
*/
BEGIN

SELECT *
FROM(

select distinct  gt.taskid as GroupTaskid, + '(' +  opa. fhanumber + ') ' + PA.ProjecttypeName as ProjectActionName,CASE
     WHEN opa. requeststatus =  6 THEN 'Draft'
     WHEN opa. requeststatus =  1 THEN 'Submit'
     ELSE ''
  END as Status ,CASE
    WHEN gt. InUse >  0 THEN roles.rolename
	ELSE '' End as [role], auth.firstname + ' ' + auth.lastname as InUseuser, gt.createdon as CreatedOn ,CASE
     WHEN gt. InUse >  0 THEN 'UnLock'
     ELSE ''
  END as UnLock
  ,opa.ServicerSubmissionDate
 from
[dbo].[Prod_GroupTasks] gt
left join
[$(LiveDB)].dbo.opaform opa
on gt. taskinstanceid=opa.taskinstanceid
left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA

on PA. ProjecttypeId=opa.Projectactiontypeid

left join [$(LiveDB)].dbo.HCP_Authentication auth
on auth.UserId=gt.InUse
left JOIN [$(LiveDB)].dbo.webpages_UsersInRoles uir ON auth . UserID = uir .UserId
left JOIN [$(LiveDB)].dbo.webpages_Roles roles ON roles . RoleId = uir .RoleId
    
	where  opa.requeststatus=6 and    gt.createdby   =@UserId  and opa.fhanumber in(select fhanumber from [$(LiveDB)].dbo.Prod_FHANumberRequest fhareq)
and roles.rolename not in ('ProductionUser','ProductionWlm','HUDAdmin','AccountExecutive','HUDDirector','WorkflowManager','Attorney','InternalSpecialOptionUser','Reviewer') -- to exclude hud user tasks
   

	UNION


select distinct  gt.taskid as GroupTaskid, + '(' +  opa. fhanumber + ') ' + PA.ProjecttypeName as ProjectActionName,CASE
     WHEN opa. requeststatus =  6 THEN 'Draft'
	   WHEN opa. requeststatus =  1 THEN 'Submit'
     ELSE ''
  END as Status ,CASE
    WHEN gt. InUse >  0 THEN roles.rolename
	ELSE '' End as [role], auth.firstname + ' ' + auth.lastname as InUseuser, gt.createdon as CreatedOn ,CASE
     WHEN gt. InUse >  0 THEN 'UnLock'
     ELSE ''
  END as UnLock
  ,opa.ServicerSubmissionDate
 from
[dbo].[Prod_GroupTasks] gt
left join
[$(LiveDB)].dbo.opaform opa
on gt. taskinstanceid=opa.taskinstanceid
left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA

on PA. ProjecttypeId=opa.Projectactiontypeid

left join [$(LiveDB)].dbo.HCP_Authentication auth
on auth.UserId=gt.InUse
left JOIN [$(LiveDB)].dbo.webpages_UsersInRoles uir ON auth . UserID = uir .UserId
left JOIN [$(LiveDB)].dbo.webpages_Roles roles ON roles . RoleId = uir .RoleId
    
	where   opa.requeststatus=6 --and    gt.createdby   =@UserId  
	and opa.fhanumber in(select fhanumber from [$(LiveDB)].dbo.Prod_FHANumberRequest fhareq where fhareq.lenderid=@LenderId)
   and roles.rolename not in ('ProductionUser','ProductionWlm','HUDAdmin','AccountExecutive','HUDDirector','WorkflowManager','Attorney','InternalSpecialOptionUser','Reviewer') -- to exclude hud user tasks
   


  UNION
  

  --Get All Save checkin records for LenderId
select distinct  gt.taskid as GroupTaskid, + '(' +  opa. fhanumber + ') ' + PA.ProjecttypeName as ProjectActionName,CASE
     WHEN opa. requeststatus =  6 THEN 'Draft'
	   WHEN opa. requeststatus =  1 THEN 'Submit'
     ELSE ''
	END as Status ,
    '' as [role], 
	'' as InUseuser, 
	gt.createdon as CreatedOn ,
	 ''  as UnLock,
	opa.ServicerSubmissionDate
 from
	[dbo].[Prod_GroupTasks] gt
	left join
	[$(LiveDB)].dbo.opaform opa
	on gt. taskinstanceid=opa.taskinstanceid
	left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA

	on PA. ProjecttypeId=opa.Projectactiontypeid
	where   opa.requeststatus=6 and    gt.InUse   =0  and gt.pagetypeid != 16
	and opa.fhanumber in(select fhanumber from [$(LiveDB)].dbo.Prod_FHANumberRequest fhareq where fhareq.lenderid=@LenderId)


   UNION

  
select gt.taskid as GroupTaskid, + '(' +  fr.ProjectName + ') ' + PA.ProjecttypeName as ProjectActionName,CASE
     WHEN fr.RequestStatus =  6 THEN 'Draft'
	 WHEN fr.RequestStatus =  1 THEN 'Submit'
	 WHEN fr.RequestStatus =  2 THEN 'Approved'
	 WHEN fr.RequestStatus =  5 THEN 'Denied'
     ELSE ''
  END as Status ,
  CASE
    WHEN gt. InUse >  0 THEN roles.rolename
	ELSE '' End as [role], 
	auth.firstname + ' ' + auth.lastname as InUseuser, gt.createdon as CreatedOn ,
	'' as UnLock
  ,fr.RequestSubmitDate
 from Prod_GroupTasks gt
join [$(LiveDB)].dbo.Prod_FHANumberRequest fr
on gt.TaskInstanceId = fr.TaskinstanceId
left join [$(LiveDB)].[dbo] .[Prod_ProjectType]  PA
on PA. ProjecttypeId=fr.ProjectTypeId
left join [$(LiveDB)].dbo.HCP_Authentication auth
on auth.UserId=gt.InUse
left JOIN [$(LiveDB)].dbo.webpages_UsersInRoles uir ON auth . UserID = uir .UserId
left JOIN [$(LiveDB)].dbo.webpages_Roles roles ON roles . RoleId = uir .RoleId     
where fr.requeststatus=6  and fr.lenderid=@LenderId
 and roles.rolename not in ('ProductionUser','ProductionWlm','HUDAdmin','AccountExecutive','HUDDirector','WorkflowManager','Attorney','InternalSpecialOptionUser','Reviewer') 
) GT
order by CreatedOn desc
END
