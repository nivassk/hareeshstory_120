﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetPAMReportStatusGrid]
AS

/*  
Date		Changed by					Reason  
====		===========					======   
04/18/2018  Suresh						open projects should be all those that have entrees in Prod_Fhanumberrequest table 
										but not completed executed closing the ones that have completed executed closing are closed   
  
*/ 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- erfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @TotalNumberOfProjects int
    Declare @TotalNumberOf223f int
    Declare @TotalNumberOf223a7 int
    Declare @TotalNumberOf223d int
    Declare @TotalNumberOf223i int
    Declare @TotalNumberOf241a int
    Declare @TotalNumberOfNC int
    Declare @TotalNumberOfSR int
	Declare @TotalNumberOfIRR int
	Declare @TotalFHAInqueue int
	Declare @TotalFHAInprocess int
	Declare @TotalFHAComplete int
	Declare @TotalWaitingApplication int
	Declare @TotalApplicationInqueue int
	Declare @TotalApplicationInprocess int
	Declare @TotalApplicationComplete int
	Declare @TotalWaitingClosing int
	Declare @TotalClosingInqueue int
	Declare @TotalClosingInprocess int
	Declare @TotalExecutedDocUploaded int
	Declare @TotalAppraiserAssigned int
	Declare @TotalEnvironmentalistAssigned int
	Declare @TotalTitleSurveyAssigned int
	Declare @Total290Assigned int
	Declare @Total290Complete int
	Declare @TotalClosing int
	Declare @LoanOpened int  
	--fisical year
	Declare  @yr  as int
	Declare  @dt  as int
	Declare  @mn  as int
	Declare @fisical as datetime

	--set fisical year
	Set @yr= YEAR(getdate())
	Set @yr= @yr-1
	Set @dt= 01
	Set @mn= 10
	Set @fisical =  CAST(CAST(@yr AS varchar) + '-' + CAST(@mn AS varchar) + '-' + CAST(@dt AS varchar) AS DATETIME)

	Set @TotalNumberOfProjects = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest)
	Set @TotalNumberOf223f = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 1)
	Set @TotalNumberOf223a7 = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 2)
	Set @TotalNumberOf223d = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 3)
	Set @TotalNumberOf223i = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 4)
	Set @TotalNumberOf241a = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 5)
	Set @TotalNumberOfNC = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 6)
	Set @TotalNumberOfSR = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 7)
	Set @TotalNumberOfIRR = (Select count(*) from [$(LiveDB)].dbo.Prod_FHANumberRequest where ProjectTypeId = 8)
	Set @TotalFHAInqueue = (Select count(distinct TaskInstanceId) from Task where PageTypeId = 4 and TaskStepId = 17)
	Set @TotalFHAInprocess = (Select count(distinct TaskInstanceId) from Task where PageTypeId = 4 and TaskStepId = 18)
	Set @TotalFHAComplete = (Select count(distinct TaskInstanceId) from Task where PageTypeId = 4 and TaskStepId = 19)
	Set @TotalWaitingApplication = (select count(distinct n.FhaNumber) from [$(LiveDB)].dbo.Prod_NextStage n
									left outer join Task t on  n.NextPgTaskInstanceId = t.TaskInstanceId
									where NextPgTypeId = 5 and t.TaskInstanceId is  null)
	Set @TotalApplicationInqueue = (select count(distinct TaskInstanceId) from Task where PageTypeId = 5 and TaskStepId = 17 and AssignedTo is null)
	Set @TotalApplicationInprocess = (select count(distinct TaskInstanceId) from Task where PageTypeId = 5 and AssignedTo is not null and TaskStepId not in(15,16,19,20,21))
	Set @TotalApplicationComplete = (select count(distinct TaskInstanceId) from Task tk 
										left join ParentChildTask pc
										on tk.TaskInstanceId = pc.ChildTaskInstanceId
										where pc.ChildTaskInstanceId is null 
										and PageTypeId = 5 and TaskStepId = 15)
	Set @TotalWaitingClosing = (select count(distinct n.FhaNumber) from [$(LiveDB)].dbo.Prod_NextStage n
								left outer join Task t on  n.NextPgTaskInstanceId = t.TaskInstanceId
								where NextPgTypeId = 10 and t.TaskInstanceId is  null)
	Set @TotalClosingInqueue = (select count(distinct TaskInstanceId) from Task where PageTypeId = 10 and TaskStepId = 17 and AssignedTo is null)
	Set @TotalClosingInprocess = (select count(distinct TaskInstanceId) from Task where PageTypeId = 10 and AssignedTo is not null and TaskStepId not in(15,16,19,20,21))
	Set @TotalExecutedDocUploaded = (select count(distinct TaskInstanceId) from Task where PageTypeId = 17 and TaskStepId = 15)
	Set @TotalAppraiserAssigned = (select count(distinct TaskInstanceId) from Prod_TaskXref where ViewId = 12 and Status in (15,18))
	Set @TotalEnvironmentalistAssigned = (select count(distinct TaskInstanceId) from Prod_TaskXref where ViewId = 3 and Status in (15,18))
	Set @TotalTitleSurveyAssigned = (select count(distinct TaskInstanceId) from Prod_TaskXref where ViewId = 2 and Status in (15,18))
	Set @Total290Assigned = (select count(distinct TaskInstanceId) from Task where PageTypeId = 16 and TaskStepId = 22)
	Set @Total290Complete = (select count(distinct TaskInstanceId) from Task where PageTypeId = 16 and TaskStepId = 23)
	SET @TotalClosing=(select count(distinct tk.FhaNumber) from  [$(DatabaseName)].dbo.Task tk   
          inner join [$(LiveDB)].dbo.Prod_FHANumberRequest pc  
          on tk.FhaNumber = pc.FhaNumber  
          where PageTypeId = 17 and TaskStepId = 15 and starttime > @fisical)--get non-cons closing

	SET @LoanOpened = @TotalNumberOfProjects - @TotalClosing

	Select @TotalNumberOfProjects as TotalNumberOfProjects, @TotalNumberOf223f as TotalNumberOf223f, @TotalNumberOf223a7 as TotalNumberOf223a7, @TotalNumberOf223d as TotalNumberOf223d,
		   @TotalNumberOf223i as TotalNumberOf223i, @TotalNumberOf241a as TotalNumberOf241a, @TotalNumberOfNC as TotalNumberOfNC, @TotalNumberOfSR as TotalNumberOfSR, @TotalNumberOfIRR as TotalNumberOfIRR,
		   @TotalFHAInqueue as TotalFHAInqueue, @TotalFHAInprocess as TotalFHAInprocess, @TotalFHAComplete as TotalFHAComplete, @TotalWaitingApplication as TotalWaitingApplication,
		   @TotalApplicationInqueue as TotalApplicationInqueue, @TotalApplicationInprocess as TotalApplicationInprocess, @TotalApplicationComplete as TotalApplicationComplete,
		   @TotalWaitingClosing as TotalWaitingClosing, @TotalClosingInqueue as TotalClosingInqueue, @TotalClosingInprocess as TotalClosingInprocess,
		   @TotalExecutedDocUploaded as TotalExecutedDocUploaded, @TotalAppraiserAssigned as TotalAppraiserAssigned, @TotalEnvironmentalistAssigned as TotalEnvironmentalistAssigned,
		   @TotalTitleSurveyAssigned as TotalTitleSurveyAssigned, @Total290Assigned as Total290Assigned, @Total290Complete as Total290Complete,
		   @LoanOpened as LoanOpened, @TotalClosing as LoanClosed
END
RETURN 0