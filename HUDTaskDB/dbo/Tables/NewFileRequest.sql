﻿
CREATE TABLE [dbo].[NewFileRequest](
	[NewFileRequestId] [int] IDENTITY(1000,1) NOT NULL,
	[ChildTaskInstanceId] [uniqueidentifier] NOT NULL,
	[Comments] [varchar](500) NOT NULL,
	[RequestedBy] [int] NOT NULL,
	[RequestedOn] [datetime] NOT NULL,
	[ReviewerProdViewId] [int] NULL,
 CONSTRAINT [PK_NewFileRequest] PRIMARY KEY CLUSTERED 
(
	[NewFileRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]