﻿
CREATE TABLE [dbo].[RequestAdditionalInfoFiles](
	[RequestAdditionalFileId] [uniqueidentifier] NOT NULL,
	[TaskFileId] [uniqueidentifier] NOT NULL,
	[ChildTaskInstanceId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_RequestAdditionalInfoFiles] PRIMARY KEY CLUSTERED 
(
	[RequestAdditionalFileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RequestAdditionalInfoFiles]  WITH CHECK ADD  CONSTRAINT [FK_RequestAdditionalInfoFiles_RequestAdditionalInfoFiles] FOREIGN KEY([RequestAdditionalFileId])
REFERENCES [dbo].[RequestAdditionalInfoFiles] ([RequestAdditionalFileId])
GO

ALTER TABLE [dbo].[RequestAdditionalInfoFiles] CHECK CONSTRAINT [FK_RequestAdditionalInfoFiles_RequestAdditionalInfoFiles]
GO

