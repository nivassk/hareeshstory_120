﻿CREATE TABLE [dbo].[FirmCommitmentAmendmentType](
	[FirmCommitmentAmendmentTypeId] [INT] IDENTITY(1,1) NOT NULL,
	[FirmCommitmentAmendment] [NVARCHAR](5) ,
	[CreatedOn] [DATETIME] NULL,
	[ModifiedOn] [DATETIME] NULL,
 CONSTRAINT [PK_FirmCommitmentAmendmentType] PRIMARY KEY CLUSTERED 
(
	[FirmCommitmentAmendmentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]