﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace Model
{
    public class PortfolioSummaryModel
    {
        public PaginateSortModel<DerivedUploadData> PortfolioModel { get; set; }
        public YearQuarterModel YearQrtModel { get; set; }
    }
}
