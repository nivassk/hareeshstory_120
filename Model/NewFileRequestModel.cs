﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class NewFileRequestModel
    {
        public int NewFileRequestId;
        public Guid ChildTaskInstanceId;
        public string Comments;
        public int RequestedBy;
        public DateTime RequestedOn;
        public int ReviewerProdViewId;
    }
}
