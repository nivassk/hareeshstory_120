﻿using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model.InternalExternalTask
{
   public class InternalExternalTaskModel
    {
        public int InternalExternalTaskID { get; set; }

        public Guid TaskInstanceID { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Task Type")]
        public int PageTypeID { get; set; }
        public string PageType { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please Select User Type")]
        public int UserTypeID { get; set; }
        public string UserType { get; set; }

        [Required(ErrorMessage = "Please Select FHA Number")]
        public string FHANumber { get; set; }     
        public int ? AssignedToUserID { get; set; }
        public string  AssignedToUserName { get; set; }
        public int ? LenderID { get; set; }
        public string LenderName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ? ModifiedOn { get; set; }
        public int ? ModifiedBy { get; set; }
        public DateTime ? ForwardedDate { get; set; }
        public DateTime ? ClosingDate { get; set; }
        public string CommentVerbage { get; set; }
        public string AdditionalComments { get; set; }
        public AdditionalPropertyInfo PropertyInfo { get; set; }
        public DateTime  ? SurveyDate { get; set;}
        public int StatusID { get; set; }
        public string Status { get; set; }
        public int ?  InUse { get; set; }
        public string SelectedFhaNumber { get; set; }
        public string SelectedUserName { get; set; }
    }
    //public class NOI:InternalExternalTaskModel
    //{
    //    public DateTime SurveyDate { get; set; }
    //}
   
    //public class REAC : InternalExternalTaskModel
    //{
    //    public int Reac_Inspection_Score { get; set; }
    //    //:TODO List all properties of Reac

    //}
}