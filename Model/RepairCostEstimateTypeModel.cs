﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
	public class RepairCostEstimateTypeModel
	{
		public int RepairCostEstimateTypeId { get; set; }
		public string RepairCostEstimate { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}

