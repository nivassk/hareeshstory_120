﻿using System;

namespace HUDHealthcarePortal.Model
{
    public partial class UploadDataSummary
    {
        public System.DateTime DataInserted { get; set; }
        public string Lender_Name { get; set; }
        public Nullable<int> LenderID { get; set; }
        public string UserName { get; set; }
    }
}
