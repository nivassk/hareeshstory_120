﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public  class ProdGroupTaskModel
    {
        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int RequestStatus { get; set; }
        public int? InUse { get; set; }
        public int PageTypeId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public bool IsDisclaimerAccepted { get; set; }
    }
}
