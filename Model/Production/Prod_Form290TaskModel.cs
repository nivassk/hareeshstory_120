﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
   public class Prod_Form290TaskModel
    {
        public int Form290TaskID { get; set; }
        public Guid ClosingTaskInstanceID { get; set; }
        public Guid TaskInstanceID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedByUserName { get; set; }
        public int Status { get; set; }
    }
}
