﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Model.Production
{
   public class Prod_EmailToLenderModel
    {
       public string SendToEmail { get; set; }
       public SelectList CCToEmailList { get; set; }
       public SelectList Emailtemplate { get; set; }
       public List<string> CCEmails { get; set; }
       public string OtherEmails { get; set; }
       public string EmailTemplateId { get; set; }
       public Guid TaskInstanceID { get; set; }   
       public string PortalAddress { get; set; }
       public string ProjectName { get; set; }
       public int ProjectNumber { get; set; }
       public string ClosingCoordinatorName  { get; set; }
       public string ConstructionManagerName { get; set; }
       public DateTime ClosingDate { get; set; }
       public string emailStatus { get; set; }
       public string AccountExecutive { get; set; }
       private string emailReceipantType = "Default";
       public string EmailReceipantType {
			get	{ return emailReceipantType; }
			set { emailReceipantType = value; }
		} 
      

    }
    public class prod_EmailToProdUser : Prod_EmailToLenderModel
    {
        public List<string> ToProdUsers { get; set; }
    }

}
