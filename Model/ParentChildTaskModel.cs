﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;

namespace HUDHealthcarePortal.Model
{
    public class ParentChildTaskModel
    {
        public Guid ParentChildTaskId { get; set; }
        public Guid ChildTaskInstanceId { get; set; }
        public Guid ParentTaskInstanceId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
    }
}
